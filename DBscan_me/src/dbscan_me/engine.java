/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dbscan_me;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFrame;
public class engine extends JFrame{
    public points[] actual;
     JFrame picture=new JFrame();
    public points temp;
    public double eps;
    public double minpts;
    public List<points> not_visited = new ArrayList<points>();
    public List<points> visited = new ArrayList<points>();
    //public List<points> core_points = new ArrayList<points>();
    //public List<points> border_points = new ArrayList<points>();
    public List<points> neighborpts = new ArrayList<points>();
    public List<points> noise=new ArrayList<points>();
    public List<points> involved=new ArrayList<points>();
     
   
     int clust_num=0;
    public List<points> clusters[]=new ArrayList[20];
  
    
    public engine(List<points> x,double radius,double minpoints){
        this.not_visited=x;

        eps=radius;
        minpts=minpoints;
       for(int n = 0; n < 20; n ++) 
       {
           clusters[n] = new ArrayList();
       }
       for(int i=0;i<not_visited.size();i++){
      // System.out.println("hioi "+not_visited.get(i).get_x()+"i="+i);
       }
          
            start();
    }
    public void start(){
        System.out.println("in Start() eps="+eps+ "minpts="+minpts+"size of notvisited="+not_visited.size());
        int siz=not_visited.size();
       for(int i=0;i<siz;i++){
         //  System.out.println(not_visited.get(i).get_x());
            if(!visited.contains(not_visited.get(i)))
            {
                //System.out.println("hl");
                visited.add(not_visited.get(i));
                neighborpts=regionQuery(not_visited.get(i));
                 
                if(minpts>neighborpts.size()){
                    noise.add(not_visited.get(i));
                }
                else{
                   clusters[clust_num]=expandarea(not_visited.get(i),neighborpts);
                clust_num++;
                }
                    
                    
            }
        }
       clusters[clust_num]=noise;
       // System.out.println("noise(size)="+noise.size()+"\ncluster[0]="+clusters[0].size()+"\ncluster[1]="+clusters[1].size()+"\ncluster[2]="+clusters[2].size()+"\ncluster[3]="+clusters[3].size()+"\ncluster[4]="+clusters[4].size()+"\ncluster[5]="+clusters[5].size()+"\ncluster[6]="+clusters[6].size()+"\ncluster[7]="+clusters[7].size()+"\ncluster[8]="+clusters[8].size());
   
    }
    public List <points>expandarea(points P,List<points> neighbors ){
          List<points> cluster=new ArrayList<points>();
          cluster.add(P);
          involved.add(P);
          List<points> neighb=new ArrayList<points>();
          for(int i=0;i<neighbors.size();i++){
              if(!visited.contains(neighbors.get(i))){
                  visited.add(neighbors.get(i));
                  neighb=regionQuery(neighbors.get(i));
                  if(neighb.size()>minpts)
                      neighbors.addAll(neighb);//get core_points
              }
            if(!involved.contains(neighbors.get(i))){
                involved.add(neighbors.get(i));
                cluster.add(neighbors.get(i));
                
            }
          }
          
          return cluster;
    }
    public List<points> regionQuery(points x){
        int num=0;
        List<points> neighbour=new ArrayList<points>();
        for(int i=0;i<not_visited.size();i++){
            if(distance(x,not_visited.get(i))){
                neighbour.add(not_visited.get(i));
                }
        }
        return neighbour;
    }
    public boolean distance(points one,points two ){
       if( eps>Math.sqrt(Math.pow((one.get_x()-two.get_x()),2)+Math.pow((one.get_y()-two.get_y()),2))){
          return true;
       }
       else  return false;
      
    }
    public List<points>[] get_cluster(){
        return clusters;
    }
    
}
