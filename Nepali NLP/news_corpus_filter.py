# -*- coding: utf-8 -*-
from gensim import corpora, models, similarities
import os
import sys
import logging 

reload(sys)
sys.setdefaultencoding('utf-8')

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


if (os.path.exists("tmp/news_new.dict")):
	dictionary = corpora.Dictionary.load('tmp/news_new.dict')
	corpus = corpora.MmCorpus('tmp/news_new.mm')
	print("Used files generated from first tutorial")
else:
	print("Please run first tutorial to generate data set")

lsi = models.LsiModel(corpus, id2word=dictionary, num_topics=2)
doc = "ऋण"
vec_bow = dictionary.doc2bow(doc.lower().split())
vec_lsi = lsi[vec_bow]
print(vec_lsi)
index = similarities.MatrixSimilarity(lsi[corpus])
sims = index[vec_lsi]
sims = sorted(enumerate(sims), key=lambda item: -item[1])
print sims
lsi.print_topics(2)
print dictionary.id2token[45]