#-*- coding: utf-8 -*-
from pymongo import MongoClient
import numpy as np
import sys
import pandas as pd
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, A4
from reportlab.lib import units
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

pdfmetrics.registerFont(TTFont('Gargi', '/usr/share/fonts/truetype/Gargi/Gargi.ttf'))
 

reload(sys)
sys.setdefaultencoding('utf-8')
#print "enter topic to search"
#a=raw_input()

file_path="Lok Man Karki.pdf"

done_link_list=[]
not_done_list=[]
client=MongoClient("mongodb://192.241.170.140:65000/")
mydb=client["all_news"]


def textDraw(maka,x_b,x_e,y_b,y_e,size,text,font_type,check_value=14):

	textobj=maka.beginText(x_b,y_e)

	test_obj=maka.beginText(x_b,y_e)
	test_obj.setTextRenderMode(3)
	test_obj.setFont(font_type,size)

	textobj.setFont(font_type,size)

	text_s=text.split()

	a=size+(size/2)
	data_new=""

	for data in text_s:

		test_obj.textOut(data+" ")

		x=test_obj.getX()

		##print "testko="+str(x)

		if (int(x))>(x_e):

			textobj.setTextOrigin(x_b,(y_e-a))

			test_obj.setTextOrigin(x_b,(y_e-a))
			
			#test_obj.textOut(data+" ")

			##print "new line"

		if int(textobj.getY())>y_b:

			#print "texxxtko="+str(textobj.getX())

			textobj.textOut(data+" ")
			y_e=textobj.getY()


		if y_e<=check_value:

			data_new+=data+" "
	print y_e		
	maka.drawText(textobj)
	if data_new!="":
		maka.showPage()
		newData=textDraw(maka,20,520,10,810,12,str(data_new),"Gargi",18)
	#print "drew"
	#print data_new
	return data_new
	
if __name__=="__main__":
	count=0
	#for data in total:
	total=mydb.news_collection.find({'$or':[{'$and':[{"news_content":{'$regex':"कार्की"}},{"news_content":{'$regex':"अख्तियार"}}]}, {"news_content":{'$regex':"लोकमानसिंह"}},{"news_content":{'$regex':"लोकमान"}}]})
	file_path="Lok Man Karkis.pdf"
	maka=canvas.Canvas(file_path, pagesize=A4)
	for data in total:
		try:
			title=data["title"]
		except:
			title=data["tilte"]
		a="http://www.ratopati.com/news/"
		content="Title: "+title
		textDraw(maka,20,850,800,810,12,str(content),"Gargi")
		content="Date: "+data["news_date"]
		textDraw(maka,20,850,780,790,12,str(content),"Gargi")
		content="Location: "+data["news_location"]
		textDraw(maka,20,850,760,770,12,str(content),"Gargi")
		content="Author: "+data["author"]
		textDraw(maka,20,850,740,750,12,str(content),"Gargi")
		content="Source: "+data["source"]
		textDraw(maka,20,850,720,730,12,str(content),"Gargi")
		if data["source"]=="rato_pati_news":
			content="News Link: "+a+data["news_link"]
		else:
			content="News Link: "+data["news_link"]
		textDraw(maka,20,850,700,710,12,str(content),"Gargi")
		content=data["news_content"]
		newData=textDraw(maka,20,520,10,680,12,str(content),"Gargi",14)
		maka.showPage()
	maka.save()
	#files=open("not_done_list.txt","w")
	#for links in not_done_list:
	#	files.write(links+"\n")
	#filess=open("done_link_list.txt","w")
	#for i in done_link_list:
	#	filess.write(i+"\n")