import java.util.Calendar;
import java.util.GregorianCalendar;

public class TimeCalculator{
    Calendar calendar = new GregorianCalendar();
    private int initialHour;
    private int initialMinute;
    private int initialSecond;
    private int hour;
    private int minute;
    private int second;
    TimeCalculator(){
        setTime();
    }
    public void setTime(){
        hour=0;
        minute=0;
        second=0;
        initialHour = 0;//calendar.get(Calendar.HOUR);
        initialMinute =0; calendar.get(Calendar.MINUTE);
        initialSecond = 0;calendar.get(Calendar.SECOND);
    }
    public String getTimeDiff(){
        calendar = new GregorianCalendar();
        //hour = /*calendar.get(Calendar.HOUR)*/hour - initialHour;
        //minute = /*calendar.get(Calendar.MINUTE)*/ - initialMinute;
        second = /*calendar.get(Calendar.SECOND)*/second - initialSecond;
        second++;
        if(second>60)
        {
            minute++;
            if(minute>60){minute=0;}
            second=0;
        }
        return String.format("%02d:%02d:%02d",hour,minute,second);
    }
}
    