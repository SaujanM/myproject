/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package solarsystem;

/**
 *
 * @author saujan maka
 */
public class CreateEllipse {
     float x_r;
    float y_r;
    float z_r;
    //center of the sun
    float[] center={350,250,250};
    
   
    float[][] ellipse=new float[4][360];
    

    public float[][] generate_ellipse_coordinate(float x_radius,float y_radius,float z_radius)
    {
        int l,k;
        l=0;k=0;
        for(int i=-180;i<180;i+=1)
        {
                float x_var=x_radius*(float)Math.cos((3.14*i)/180);
                float y_var=0;
                float z_var=z_radius*(float)Math.sin((3.14*i)/180);
                ellipse[k][l]=x_var+center[0];
                k++;
                ellipse[k][l]=y_var+center[1];
                k++;
                ellipse[k][l]=z_var+center[2];
                k++;
                ellipse[k][l]=1;
                k=0;l++;
        }
        return ellipse;
    }
    public float[][] get_updated_ellipse_points_of_moon(float[]center_of_earth)
    {
        int l,k;
        l=0;k=0;
        for(int i=-180;i<180;i+=1)
        {
                float x_var=0;
                float y_var=30*(float)Math.cos((3.14*i)/180);
                float z_var=50*(float)Math.sin((3.14*i)/180);
                ellipse[k][l]=x_var+center_of_earth[0];
                k++;
                ellipse[k][l]=y_var+center_of_earth[1];
                k++;
                ellipse[k][l]=z_var+center_of_earth[2];
                k++;
                ellipse[k][l]=1;
                k=0;l++;
        }
        return ellipse;
    }
    
    
}
