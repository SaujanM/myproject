package solarsystem;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author saujan maka
 */
import java.awt.Color;
import java.util.Vector;

public class CreateSphere extends CreateEllipse {
    public float [][] world_coordinate_of_planet= new float[4][2592];
     public float [][] copy_world_coordinate_of_planet= new float[4][2592];
      float[][] ellipse=new float[4][360];
      Vector<triangle_vertices>surfaces=new Vector<triangle_vertices>();     
         
     public CreateSphere(float x_r,float y_r,float z_r)
     {
         //generate_ellipse_points.
         ellipse=generate_ellipse_coordinate(x_r,y_r,z_r);
         
         
     }
    public void generate_coordinate_of_planet(float radius)
    {
        
        //int dtheta=20;int dphi=20;
        int k,l;
        k=0;
        l=0;
        float x_var,y_var,z_var;
        
        for(int i=-90;i<90;i+=5)
        {
            for(int j=-180;j<180;j+=5)
            {
              
                 x_var=radius*(float)Math.cos((3.14*i)/180)*(float)Math.cos((j*3.14)/180);
                 y_var=radius*(float)Math.cos((3.14*i)/180)*(float)Math.sin((j*3.14)/180);
                 z_var=radius*(float)Math.sin((3.14*i)/180);
                //copy_world_coordinate_of_planet[k][l]=x_var+ellipse_point[0];
                world_coordinate_of_planet[k][l]=x_var;
                k++;
                //copy_world_coordinate_of_planet[k][l]=y_var+ellipse_point[1];
                world_coordinate_of_planet[k][l]=y_var;
                k++;
                //copy_world_coordinate_of_planet[k][l]=z_var+ellipse_point[2];//for the initial display of the sphere
                world_coordinate_of_planet[k][l]=z_var;//for the calculation of the translation of the sphere and other purposes
                k++;
                world_coordinate_of_planet[k][l]=1;
                k=0;
                l++;
               
            }
            
        }
        //return world_coordinate_of_planet;
    }
    public float[][] get_ellipse_points()
    {
        return ellipse;
    }
    
    //continuous ellipse_point updating and display ko lagi
  public float[][] translating_planet(float[]ellipse_point,float thita)
    {
        int k1,l1;
        k1=0;
        l1=0;
        float[][] result=new float [4][2592];
        double mat[][] = {{1,0, 0, 0}, {0,Math.cos((thita*Math.PI) / 180), -Math.sin((thita*Math.PI) / 180), 0},{0,Math.sin((thita*Math.PI) / 180),Math.cos((thita*Math.PI) / 180), 0}, {0, 0, 0, 1}};
      for(int k=0;k<2592;k++){
      double cmat[] = {world_coordinate_of_planet[0][k],world_coordinate_of_planet[1][k],world_coordinate_of_planet[2][k], 1};
        //double result[][] = new double[4][2592];
        double sum;
        for (int i = 0; i < 4; i++) {
            sum = 0;
            for (int j = 0; j < 4; j++) {
                sum = sum + mat[i][j] * cmat[j];
            }
            
            result[i][k] = (float)sum;
        }
        
      }
       for(int l=0;l<2592;l++)
        {
            int k=0;
             world_coordinate_of_planet[k][l]=(float)result [k][l];
             k++;
             world_coordinate_of_planet[k][l]=(float)result [k][l];
             k++;
             world_coordinate_of_planet[k][l]=(float)result [k][l];
           
        }
        
        int k,l;
        k=0;
        l=0;
        
        for(l=0;l<2592;l++)
        {
            
             copy_world_coordinate_of_planet[k][l]=world_coordinate_of_planet[k][l]+ellipse_point[0];
             k++;
             copy_world_coordinate_of_planet[k][l]=world_coordinate_of_planet[k][l]+ellipse_point[1];
             k++;
             copy_world_coordinate_of_planet[k][l]=world_coordinate_of_planet[k][l]+ellipse_point[2];
             k++;
             copy_world_coordinate_of_planet[k][l]=world_coordinate_of_planet[k][l];
            k=0;
        }
       
        return copy_world_coordinate_of_planet;

    
    }
  public void make_surface_points(float [][]perspective_coordinate2)
  {
       float x1,y1,z1,x2,y2,z2,x3,y3,z3;
       int p=0;
           triangle_vertices polygon=new triangle_vertices();
       while(p<2519)
        { 
          int temp=p; 
          x1=perspective_coordinate2[0][p];
          y1=perspective_coordinate2[1][p];
          z1=perspective_coordinate2[2][p];
          //System.out.println(p+",");
          p=p+72;
          
          x2=perspective_coordinate2[0][p];
          y2=perspective_coordinate2[1][p];
          z2=perspective_coordinate2[2][p];
           //System.out.println(p+",");
          p=p+1;
          x3=perspective_coordinate2[0][p];
          y3=perspective_coordinate2[1][p];
          z3=perspective_coordinate2[2][p];
          int[]x={(int)x1,(int)x2,(int)x3};
          int[]y={(int)y1,(int)y2,(int)y3};
           //System.out.println(p+"\n");
          
          polygon.set_triangle_vertices(x1,y1,z1,x2,y2,z2,x3,y3,z3);
          surfaces.add(polygon);
          
          //insert in surface table
          p=temp;
          x1=perspective_coordinate2[0][p];
          y1=perspective_coordinate2[1][p];
          z1=perspective_coordinate2[2][p];
           //System.out.println(p+",");
          p=p+1;
          
          x2=perspective_coordinate2[0][p];
          y2=perspective_coordinate2[1][p];
          z2=perspective_coordinate2[2][p];
           //System.out.println(p+",");
          p=p+72;
          x3=perspective_coordinate2[0][p];
          y3=perspective_coordinate2[1][p];
          z3=perspective_coordinate2[2][p];
           //System.out.println(p+"\n"); 
          polygon.set_triangle_vertices(x1,y1,z1,x2,y2,z2,x3,y3,z3);
          
          surfaces.add(polygon);
          p=temp+1;
          //surfaces.get(temp)
              
            
        }
  }
  class triangle_vertices{
      float x_1,y_1,z_1;
         float x_2,y_2,z_2;
          float x_3,y_3,z_3;
          void set_triangle_vertices(float x1,float y1,float z1,float x2,float y2,float z2,float x3,float y3,float z3)
          {
              x_1=x1;
              y_1=y1;
              z_1=z1;
              
              x_2=x2;
              y_2=y2;
              z_2=z2;
              
              x_3=x3;
              y_3=y3;
              z_3=z3;
          }
  }
}