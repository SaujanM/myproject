/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package solarsystem;

/**
 *
 * @author RAVI
 */
public class _3D_point {
    double x,y,z;
    double magnitude;
    public _3D_point()
    {
    }
    public _3D_point(double a, double b, double c){
        x = a;
        y = b;
        z = c;
        magnitude=0;
    }
    public _3D_point create_normal(_3D_point a,_3D_point b ){
        //float[] norm_vector = new float[3];
        _3D_point norm_vector = new _3D_point();
        //float A,B,C;
        norm_vector.x = a.x-b.x;
        norm_vector.y = a.y-b.y;
        norm_vector.z= a.z-b.z;
        norm_vector.magnitude=(Math.sqrt((norm_vector.x*norm_vector.x)+(norm_vector.y*norm_vector.y)+(norm_vector.z*norm_vector.z)));
        norm_vector.x=norm_vector.x/norm_vector.magnitude;
        norm_vector.y=norm_vector.y/norm_vector.magnitude;
        norm_vector.z=norm_vector.z/norm_vector.magnitude;
        return norm_vector;
    }
    public float dot_product(_3D_point p, _3D_point q){
        float prod;
        prod = (float) (p.x*q.x + p.y*q.y + p.z*q.z);
        return prod;
    }
    public static _3D_point create_normal_origin(_3D_point a,float[] init_pt ){
        //float[] norm_vector = new float[3];
        _3D_point norm_vector = new _3D_point();
        float p,q,r;
//        norm_vector.x = a.y*(b.z - c.z)+b.y*(c.z - a.z)+c.y*(a.z - b.z);
//        norm_vector.y = a.z*(b.x-c.x)+b.z*(c.x-a.x)+c.z*(a.x-b.x);
//        norm_vector.z= a.x*(b.y-c.y)+b.x*(c.y-a.y)+c.x*(a.y-b.y);
//        System.out.println("normal is"+norm_vector.x + norm_vector.y + norm_vector.z);
        norm_vector.x = (init_pt[0]-a.x);
        norm_vector.y = (init_pt[1]-a.y);
        norm_vector.z = (init_pt[2]-a.z);
        norm_vector.magnitude=(Math.sqrt((norm_vector.x*norm_vector.x)+(norm_vector.y*norm_vector.y)+(norm_vector.z*norm_vector.z)));
        norm_vector.x=norm_vector.x/norm_vector.magnitude;
        norm_vector.y=norm_vector.y/norm_vector.magnitude;
        norm_vector.z=norm_vector.z/norm_vector.magnitude;
        return norm_vector;
    }
    public _3D_point unit(_3D_point norm_vector){
        norm_vector.magnitude=(Math.sqrt((norm_vector.x*norm_vector.x)+(norm_vector.y*norm_vector.y)+(norm_vector.z*norm_vector.z)));
        norm_vector.x=norm_vector.x/norm_vector.magnitude;
        norm_vector.y=norm_vector.y/norm_vector.magnitude;
        norm_vector.z=norm_vector.z/norm_vector.magnitude;
        return norm_vector;
}
    public _3D_point make_unit(_3D_point xer){
     _3D_point y = new _3D_point();
     y.x = (float) (xer.x/(Math.sqrt(xer.x*xer.x + xer.y*xer.y + xer.z*xer.z)));
     y.y = (float) (xer.y/(Math.sqrt(xer.x*xer.x + xer.y*xer.y + xer.z*xer.z)));
     y.z = (float) (xer.z/(Math.sqrt(xer.x*xer.x + xer.y*xer.y + xer.z*xer.z)));
     return y;
 }
}
