/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package solarsystem;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import java.util.Vector;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;

/**
 *
 * @author saujan maka
 */
public class Display extends JPanel {
    Vector<make_polygon_vertices>surfaces=new Vector<make_polygon_vertices>();
    //mercury vaane object bata ellipse points and mercury points aaucha.
    
    public float [][]moon_points=new float[4][2592];
    public float[][]moon_ellipse_point=new float[4][360];
    CreateSphere moon=new CreateSphere(0f,0f,0f);
    float depth_moon;
    float []initial_point_moon;
    int speed_of_moon;
    int theta=0;
    
    public float [][]mercury_points=new float[4][2592];
    public float[][]mercury_ellipse_point=new float[4][360];
    CreateSphere mercury=new CreateSphere(80f,350f,60f);
    float depth_mercury;
    float []initial_point_mercury;
    
    float[]initial_point_of_sun={350f,250f,250f};
     
    public float [][]mars_points=new float[4][2592];
    public float[][]mars_ellipse_point=new float[4][360];
    CreateSphere mars=new CreateSphere(250f,350f,245f);
    float depth_mars;
    int speed_of_mars;
    float[]initial_point_mars;
    //float[]initial_point_of_sun={350f,250f,250f};
   
    float []initial_point=new float[3];
    //float[] initial_point_mercury,initial_point_venus,initial_point_earth,initial_point_mars,initial_point_jupiter,initial_point_saturn,initial_point_uranus,initial_point_neptune;
    //float depth_mercury,depth_mars,depth_jupiter,depth_saturn,depth_uranus,depth_neptune;
    int speed_of_mercury;
    int delay_time = 500;
    
    float[][]venus_points=new float[4][2592];
    float[][]venus_ellipse_point=new float[4][360];
    CreateSphere venus=new CreateSphere(150f,200f,130f);
    float depth_venus;
    int speed_of_venus;
    float[]initial_point_venus;
     
      
     float[][]earth_points=new float[4][2592];
     float[][]earth_ellipse_point=new float[4][360];
     CreateSphere earth=new CreateSphere(200f,350f,190f);
     float depth_earth;
     int speed_of_earth;
     float[]initial_point_earth;
     
      float[][]jupiter_points=new float[4][2592];
     float[][]jupiter_ellipse_point=new float[4][360];
     CreateSphere jupiter=new CreateSphere(269f,350f,310f);
     float depth_jupiter;
     int speed_of_jupiter;
     float[]initial_point_jupiter;
     
      float[][]saturn_points=new float[4][2592];
     float[][]saturn_ellipse_point=new float[4][360];
     CreateSphere saturn=new CreateSphere(320f,350f,380f);
     float depth_saturn;
     int speed_of_saturn;
     float[]initial_point_saturn;
     
      float[][]uranus_points=new float[4][2592];
     float[][]uranus_ellipse_point=new float[4][360];
     CreateSphere uranus=new CreateSphere(340f,350f,420f);
     float depth_uranus;
     int speed_of_uranus;
     float[]initial_point_uranus;
     
      float[][]neptune_points=new float[4][2592];
     float[][]neptune_ellipse_point=new float[4][360];
     CreateSphere neptune=new CreateSphere(360f,350f,450f);
     float depth_neptune;
     int speed_of_neptune;
     float[]initial_point_neptune;
     
      float[][]pluto_points=new float[4][2592];
     float[][]pluto_ellipse_point=new float[4][360];
     CreateSphere pluto=new CreateSphere(400f,350f,480f);
     float depth_pluto;
     int speed_of_pluto;
     float[]initial_point_pluto;
      float[][]sun_points=new float[4][2592];
     //float[][]earth_ellipse_point=new float[4][360];
     CreateSphere sun=new CreateSphere(0f,0f,0f);
     //float depth_sun;
     //int speed_of_earth;
     float [][] center =new float[4][1];
     float actual_sun_pos = 100f;
     
     float[] sorting=new float[11];
     float u=0;
     float z_vp=50;//view plane is taken to be the uv plane,then z_vp=0
     float z_prp=1080;//greater the distance,smaller the object will appear
     float x_prp=1080;
     float y_prp=-140;
     float dp=z_prp-z_vp;
     float kd=0.6f,ks=0.3f,ka=0.8f;
      
    public Display()
    {
        //moon_ellipse_point=neptune.get_ellipse_points();
      speed_of_moon=150;
      moon.generate_coordinate_of_planet(5f);
        
      speed_of_mercury=200;
      mercury_ellipse_point=mercury.get_ellipse_points();
      mercury.generate_coordinate_of_planet(7f);//generate world_coordinate of planet with center (0,0,0) and still rotated and translated
      
      venus_ellipse_point=venus.get_ellipse_points();
      speed_of_venus=175;
      venus.generate_coordinate_of_planet(20f);//generate world_coordinate of planet with center (0,0,0) and still rotated and translated
      
     // speed_of_mercury=200;
      //mercury_ellipse_point=mercury.get_ellipse_points();
      
      sun.generate_coordinate_of_planet(50f);
 
      earth_ellipse_point=earth.get_ellipse_points();
      speed_of_earth=120;
      earth.generate_coordinate_of_planet(20f);//generate world_coordinate of planet with center (0,0,0) and still rotated and translated

      mars_ellipse_point=mars.get_ellipse_points();
      speed_of_mars=105;
      mars.generate_coordinate_of_planet(10f);
      
      jupiter_ellipse_point=jupiter.get_ellipse_points();
      speed_of_jupiter=95;
      jupiter.generate_coordinate_of_planet(26f);
      
      saturn_ellipse_point=saturn.get_ellipse_points();
      speed_of_saturn=90;
      saturn.generate_coordinate_of_planet(24f);
      
     uranus_ellipse_point=uranus.get_ellipse_points();
      speed_of_uranus=75;
      uranus.generate_coordinate_of_planet(22f);
      
      neptune_ellipse_point=neptune.get_ellipse_points();
      speed_of_neptune=70;
      neptune.generate_coordinate_of_planet(23f);
      
      pluto_ellipse_point=pluto.get_ellipse_points();
      speed_of_pluto=30;
       pluto.generate_coordinate_of_planet(3f);
            int delay = 100; //milliseconds
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                repaint();
                //System.out.println(i++);
            }
        };
        new Timer(delay, taskPerformer).start();
          this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e){
                //Here the problem occurs, the layOutPanel[] is not recognized.
             if (e.isMetaDown()){y_prp=y_prp-60;
                 //System.out.println(y_prp+"y-prrp,");
             }else{y_prp=y_prp+60;}
                //System.out.println(x_prp+"y_prp"+y_prp);
//                actual_sun_pos=actual_sun_pos+10;}
//                else {actual_sun_pos=actual_sun_pos-10;}
//                System.out.println("actual_sun_pos="+actual_sun_pos);
//                System.out.println("note y_prp="+y_prp);
//                 System.out.println("note z_prp="+z_prp);
                //repaint();
                }
        });
    }
     public void paintComponent( Graphics g ){
       
        
     super.paintComponent( g );
     set_background_arrangement(g);
     //ellipse of mercury
     // this.setBackground(Color.black);
         paint_ellipse(mercury_ellipse_point,g);
         paint_ellipse(venus_ellipse_point,g);
         paint_ellipse(earth_ellipse_point,g);
         paint_ellipse(mars_ellipse_point,g);
         paint_ellipse(jupiter_ellipse_point,g);
         paint_ellipse(saturn_ellipse_point,g);
         paint_ellipse(uranus_ellipse_point,g);
         paint_ellipse(neptune_ellipse_point,g);
         paint_ellipse(pluto_ellipse_point,g);
          //paint_ellipse(moon_ellipse_point,g);
         
      //mercury 
         theta=theta+10;
         if(theta>360)theta=0;
      speed_of_mercury=speed_of_mercury+33;
      if(speed_of_mercury>350)speed_of_mercury=0;
      initial_point[0]=mercury_ellipse_point[0][speed_of_mercury];
      initial_point[1]=mercury_ellipse_point[1][speed_of_mercury];
      initial_point[2]=mercury_ellipse_point[2][speed_of_mercury];
      depth_mercury=initial_point[2];
      initial_point_mercury = initial_point;
      sorting[0]=initial_point[2];
      mercury_points =  mercury.translating_planet(initial_point,theta);
      //System.out.println("(x,y,z)"+ initial_point[0]+ initial_point[1]+ initial_point[2]);
      
      
         //System.out.println("mercury center="+"(x,y,z)"+initial_point[0]+","+initial_point[1]+","+initial_point[2] );
      
      //points with translated excluding rotation
      //venus 
      speed_of_venus=speed_of_venus+27; 
      if(speed_of_venus>350)speed_of_venus=0;
        
      initial_point[0]=venus_ellipse_point[0][speed_of_venus];
      initial_point[1]=venus_ellipse_point[1][speed_of_venus];
      initial_point[2]=venus_ellipse_point[2][speed_of_venus];
      depth_venus=initial_point[2];
      initial_point_venus = initial_point;
      sorting[1]=initial_point[2];
      venus_points =  venus.translating_planet(initial_point,theta);
      //earth 
      speed_of_earth=speed_of_earth+1;
      if(speed_of_earth>350)speed_of_earth=0;
                    
      
      initial_point[0]=earth_ellipse_point[0][speed_of_earth];
      initial_point[1]=earth_ellipse_point[1][speed_of_earth];
      initial_point[2]=earth_ellipse_point[2][speed_of_earth];
      earth_points =  earth.translating_planet(initial_point,theta);
      depth_earth=initial_point[2];
      initial_point_earth = initial_point;
      sorting[2]=initial_point[2];
      
      speed_of_moon=speed_of_moon+14;
      if(speed_of_moon>349)speed_of_moon=0;
      moon_ellipse_point=moon.get_updated_ellipse_points_of_moon(initial_point);
     // paint_ellipse(moon_ellipse_point,g);
      initial_point[0]=moon_ellipse_point[0][speed_of_moon];
      initial_point[1]=moon_ellipse_point[1][speed_of_moon];
      initial_point[2]=moon_ellipse_point[2][speed_of_moon];
      float[] moon_updated_center=new float[3];
      moon_updated_center[0]=initial_point[0];
      moon_updated_center[1]=initial_point[1];
      moon_updated_center[2]=initial_point[2];
      moon_points =  moon.translating_planet(moon_updated_center,theta);
      depth_moon=initial_point[2];
      initial_point_moon=moon_updated_center;
      sorting[10]=initial_point[2];
      
      
      //mars
      speed_of_mars=speed_of_mars+4; 
      if(speed_of_mars>350)speed_of_mars=0;
        
      initial_point[0]=mars_ellipse_point[0][speed_of_mars];
      initial_point[1]=mars_ellipse_point[1][speed_of_mars];
      initial_point[2]=mars_ellipse_point[2][speed_of_mars];
      depth_mars=initial_point[2];
      initial_point_mars = initial_point;
      sorting[4]=initial_point[2];
      mars_points =  mars.translating_planet(initial_point,theta);
      //jupiter
      speed_of_jupiter=speed_of_jupiter+2;
      if(speed_of_jupiter>350)speed_of_jupiter=0;
      initial_point[0]=jupiter_ellipse_point[0][speed_of_jupiter];
      initial_point[1]=jupiter_ellipse_point[1][speed_of_jupiter];
      initial_point[2]=jupiter_ellipse_point[2][speed_of_jupiter];
      depth_jupiter=initial_point[2];
      initial_point_jupiter = initial_point;
      sorting[5]=initial_point[2];
      jupiter_points = jupiter.translating_planet(initial_point,theta);
      //saturn
      speed_of_saturn=speed_of_saturn+4;
      if(speed_of_saturn>350)speed_of_saturn=0;
      initial_point[0]=saturn_ellipse_point[0][speed_of_saturn];
      initial_point[1]=saturn_ellipse_point[1][speed_of_saturn];
      initial_point[2]=saturn_ellipse_point[2][speed_of_saturn];
      depth_saturn=initial_point[2];
      initial_point_saturn = initial_point;
      sorting[6]=initial_point[2];
      saturn_points =  saturn.translating_planet(initial_point,theta);
      //uranus
      speed_of_uranus=speed_of_uranus+3;
      if(speed_of_uranus>350)speed_of_uranus=0;
      initial_point[0]=uranus_ellipse_point[0][speed_of_uranus];
      initial_point[1]=uranus_ellipse_point[1][speed_of_uranus];
      initial_point[2]=uranus_ellipse_point[2][speed_of_uranus];
      depth_uranus=initial_point[2];
      initial_point_uranus = initial_point;
      sorting[7]=initial_point[2];
      uranus_points =  uranus.translating_planet(initial_point,theta);
      //neptune
      speed_of_neptune=speed_of_neptune+2;
      if(speed_of_neptune>350)speed_of_neptune=0;
      initial_point[0]=neptune_ellipse_point[0][speed_of_neptune];
      initial_point[1]=neptune_ellipse_point[1][speed_of_neptune];
      initial_point[2]=neptune_ellipse_point[2][speed_of_neptune];
      depth_neptune=initial_point[2];
      initial_point_neptune = initial_point;
      sorting[8]=initial_point[2];
      neptune_points =  neptune.translating_planet(initial_point,theta);
      //pluto
      speed_of_pluto=speed_of_pluto+1;
      if(speed_of_pluto>350)speed_of_pluto=0;
      initial_point[0]=pluto_ellipse_point[0][speed_of_pluto];
      initial_point[1]=pluto_ellipse_point[1][speed_of_pluto];
      initial_point[2]=pluto_ellipse_point[2][speed_of_pluto];
      depth_pluto=initial_point[2];
      initial_point_pluto = initial_point;
      sorting[9]=initial_point[2];
      pluto_points =  pluto.translating_planet(initial_point,theta);
      
      sun_points =  sun.translating_planet(initial_point_of_sun,theta);
      sorting[3]=initial_point_of_sun[2];
      //depth_earth=initial_point[2];
      //sorting[1]=initial_point_of_sun[2];
      
        float temp=0;
        for(int i=0;i<11;i++)
        {
          for(int j=i;j<11;j++)
           {
             if(sorting[i]<sorting[j]){
                temp=sorting[i];
                sorting[i]=sorting[j];
                sorting[j]=temp;
            }
           }
       }
      
          
       for(int i=10;i>=0;i--)
    {
        if(sorting[i]==depth_earth)paint_planet_x(earth_points,g,initial_point_earth,0,100,200);
        if(sorting[i]==depth_mercury){paint_planet_x(mercury_points,g,initial_point_mercury,187,180,158); 
//        System.out.println("depth of mercury="+depth_mercury);
        }
        if(sorting[i]==depth_venus)paint_planet_x(venus_points,g,initial_point_venus,230,95,20); 
        if(sorting[i]==250f)paint_planet_sun(sun_points,g);//System.out.println("depth of sun="+sorting[i]);}
        if(sorting[i]==depth_mars)paint_planet_x(mars_points,g,initial_point_venus,226,53,5);
        if(sorting[i]==depth_jupiter)paint_planet_x(jupiter_points,g,initial_point_jupiter,163,138,131);
        if(sorting[i]==depth_saturn)paint_planet_x(saturn_points,g,initial_point_saturn,106,98,98);
        if(sorting[i]==depth_uranus)paint_planet_x(uranus_points,g,initial_point_uranus,127,154,232);
        if(sorting[i]==depth_neptune)paint_planet_x(neptune_points,g,initial_point_neptune,10,29,85);
        if(sorting[i]==depth_pluto)paint_planet_x(pluto_points,g,initial_point_pluto,270,37,27);
        if(sorting[i]==depth_moon)paint_planet_x(moon_points,g,initial_point_moon,60,60,60);
    }
//         System.out.println();
         
                    
//                    repaint();

     }
        public int[] color_range(int a, int b, int c){
         int max;
         int[] arr = new int[3];
         if(a>=b && a>=c)
             max = a;
         else if(b>=a && b>=c)
             max = b;
         else 
             max = c;
        
         //if(max <= 0)max = 255; 
            arr[0] = (a/max)*255;
            arr[1] = (b/max)*255;
            arr[2] = (c/max)*255;
         return arr;
     }
     public float[][] returns_perspective_coordinate(float z_vp,float z_prp,float dp,float [][]cube_coordinate, int loop){
      
       float multiply[][]=new float[4][loop];
       float sum=0;
       
      //float [][]pptransformation={{1,0,0,0},{0,1,0,0},{0,0,-z_vp/dp,(z_prp/dp)*z_vp},{0,0,-1/dp,z_prp/dp}};
       float [][]pptransformation={{1,0,0,((x_prp*z_vp)/(z_prp-z_vp))},{0,1,0,((y_prp*z_vp)/(z_prp-z_vp))},{0,0,-z_vp/dp,(z_prp/dp)*z_vp},{0,0,-1/dp,z_prp/dp}};
       
     
       for (int c = 0 ; c < 4 ; c++ )
       {
         for (int d = 0 ; d < loop ; d++ )
         {
           for ( int k = 0 ; k < 4 ; k++ )
           {
             sum = sum + pptransformation[c][k]*cube_coordinate[k][d];
           }
 
             multiply[c][d] = sum;
            sum = 0;
         }
       }
       return multiply;
   }
     public void draw_it(float[][] perspective_coordinate,float[][]cube_coordinate,float z_prp,float d,float loop)
   {//
       float h;
       for(int i=0;i<2;i++)
       {
           for(int j=0;j<loop;j++)
           {
               h=(z_prp-cube_coordinate[2][j])/d;
               perspective_coordinate[i][j]=(perspective_coordinate[i][j])/h;
           }
       }
}
    public void paint_ellipse(float[][]general_ellipse_points,Graphics g){
              float [][]perspective_coordinate=returns_perspective_coordinate(z_vp,z_prp,dp,general_ellipse_points,360);
             draw_it(perspective_coordinate,general_ellipse_points,z_prp,dp,360);
             for(int j=0;j<360;j++)
              {
                g.drawLine( (int)perspective_coordinate[0][j],(int) perspective_coordinate[1][j],(int) perspective_coordinate[0][j],(int)perspective_coordinate[1][j]);
              }
              
          }
     public void make_delay(int m){
        try {
            Thread.sleep(m);
        } catch (InterruptedException ex) {
            Logger.getLogger(Display.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
     void paint_planet_x(float[][]planet_world_coordinate,Graphics g,float[] initial_point,int re,int gr,int bl)
     { 
         //imported late
         float[][]perspective_coordinate_planet=new float[4][1]; 
         center[0][0]=initial_point[0];
         center[1][0]=initial_point[1];
         center[2][0]=initial_point[2];
         perspective_coordinate_planet=returns_perspective_coordinate(z_vp,z_prp,dp,center,1);
         draw_it(perspective_coordinate_planet,center,z_prp,dp,1);
         //import finish 
         
         float [][]perspective_coordinate2=returns_perspective_coordinate(z_vp,z_prp,dp,planet_world_coordinate,2592);//2592 points in the sphere with increment of 5
         draw_it(perspective_coordinate2,planet_world_coordinate,z_prp,dp,2592);
         int p=0;
         float x1,y1,z1,x2,y2,z2,x3,y3,z3;
         float a1,b1,c1,a2,b2,c2,a3,b3,c3;
         make_polygon_vertices polygon=new make_polygon_vertices();
         while(p<2519)
         { 
          int temp=p; 
          x1=perspective_coordinate2[0][p];
          a1 = mercury_points[0][p];
          y1=perspective_coordinate2[1][p];
          b1 = mercury_points[1][p];
          z1=perspective_coordinate2[2][p];
          c1 = mercury_points[2][p];
           
          //System.out.println(p+",");
          p=p+72;
          
          x2=perspective_coordinate2[0][p];
          a2 = mercury_points[0][p];
          y2=perspective_coordinate2[1][p];
          b2 = mercury_points[1][p];
          z2=perspective_coordinate2[2][p];
          c2 = mercury_points[2][p];
           //System.out.println(p+",");
          p=p+1;
          x3=perspective_coordinate2[0][p];
          a3 = mercury_points[0][p];
          y3=perspective_coordinate2[1][p];
          b3 = mercury_points[1][p];
          z3=perspective_coordinate2[2][p];
          c3 = mercury_points[2][p];
          int[]x={(int)x1,(int)x2,(int)x3};
          int[]y={(int)y1,(int)y2,(int)y3};
           //System.out.println(p+"\n");
          Color clr;
          int red,green,blue;
          
          float kd = 0.35f;
          float ka = 0.6f;
          float ks = 0.25f;
          red = re;
          green = gr;
          blue = bl;
          
          
          
          _3D_point view = new _3D_point(x_prp,y_prp,z_prp);
          //_3D_point light_src = new _3D_point(350,250,350);
          _3D_point light_src = new _3D_point(perspective_coordinate_planet[0][0],perspective_coordinate_planet[1][0],perspective_coordinate_planet[2][0]);
          view=view.unit(view);
           float[][]perspective_coordinate_sun=new float[4][1]; 
          
           ///change garnu parcha hola
           float[][] cent = new float[4][1];
            cent[0][0]=350;
            cent[1][0]=250;
            cent[2][0]=250;

                 perspective_coordinate_sun=returns_perspective_coordinate(z_vp,z_prp,dp,cent,1);
             draw_it(perspective_coordinate_sun,cent,z_prp,dp,1);
             
          // chage yehi samma yo section ko
          float [] planet =new float[3];
//          planet[0]=perspective_coordinate_sun[0][0];
          planet[0]=1;
//          planet[1]=perspective_coordinate_sun[1][0];
//          planet[2]=perspective_coordinate_sun[2][0];
          planet[0]=1;
          planet[0]=-1;
          light_src=_3D_point.create_normal_origin(light_src,planet);
          _3D_point temp1 = new _3D_point(x1,y1,z1);

          _3D_point real_temp = new _3D_point();
          _3D_point point = new _3D_point();
          real_temp = real_temp.create_normal_origin(temp1,initial_point);
          double product = real_temp.dot_product(real_temp, light_src);
         
          point=point.create_normal(temp1,view);
          real_temp=real_temp.create_normal(point, real_temp);
         double back = view.dot_product(real_temp, view);
         red = (int)(red*kd*product+red*ka+ks*re*back*back);
          green = (int)(green*kd*product+green*ka+ks*green*back*back);
          blue = (int)(blue*kd*product+blue*ka+ks*blue*back*back);
          int[]maxe=new int [3];
          if(red>255||green>255||blue>255)
          {
            maxe = this.color_range(red,green,blue);
            red= maxe[0];
            blue= maxe[2];
            green=maxe[1];
          }
         
          if(red<0){red=0;}
          if(green<0){green=0;}
          if(blue<0){blue=0;}
          
          
         
          
          
          clr = new Color(red,green,blue);
           g.setColor(clr);
          draw_surface(g,x, y, 3);
          
          p=temp;
         x1=perspective_coordinate2[0][p];
          a1 = mercury_points[0][p];
          y1=perspective_coordinate2[1][p];
          b1 = mercury_points[1][p];
          z1=perspective_coordinate2[2][p];
          c1 = mercury_points[2][p];
           //System.out.println(p+",");
          p=p+1;
          
         x2=perspective_coordinate2[0][p];
          a2 = mercury_points[0][p];
          y2=perspective_coordinate2[1][p];
          b2 = mercury_points[1][p];
          z2=perspective_coordinate2[2][p];
          c2 = mercury_points[2][p];
           //System.out.println(p+",");
          p=p+72;
          x3=perspective_coordinate2[0][p];
          a3 = mercury_points[0][p];
          y3=perspective_coordinate2[1][p];
          b3 = mercury_points[1][p];
          z3=perspective_coordinate2[2][p];
          c3 = mercury_points[2][p];
           
          int[]xx={(int)x1,(int)x2,(int)x3};
          int[]yy={(int)y1,(int)y2,(int)y3};
           //System.out.println(p+"\n");
          //Color clrs;
          //int redd,greenn,bluee;
//         red = re;
//          green = gr;
//          blue = bl;
         // _3D_point light_srcc = new _3D_point(1,1,-1);
          
             
          _3D_point light_srcc = new _3D_point(perspective_coordinate_planet[0][0],-perspective_coordinate_planet[1][0],-perspective_coordinate_planet[2][0]); 
          _3D_point temp4 = new _3D_point(x1,y1,z1);
          //_3D_point view = new _3D_point(x_prp,y_prp,z_prp);
          _3D_point real_tempo = new _3D_point();
          light_srcc=_3D_point.create_normal_origin(light_srcc,planet);
          //light_srcc=light_src.unit(light_srcc);
          real_tempo = real_tempo.create_normal_origin(temp4,initial_point);
          double productt = real_tempo.dot_product(real_tempo, light_srcc);
          //real_tempo = real_tempo.create_normal_origin(temp4,planet);
           point=point.create_normal(temp4,view);
           real_tempo=real_tempo.create_normal(point, real_tempo);
         double back1=view.dot_product(real_tempo, view);
           // System.out.println("The value of productt is" + productt);
          //float kd = 0.5f;
//          //float ka = 0.7f;
//          //productt*=10;
          
//          //productt*=10;
//          //if(productt < 0)productt = - productt;
          red = (int)(red*kd*productt + red*ka+ks*red*back1*back1);
          green = (int)(green*kd*productt + green*ka+ks*green*back1*back1);
          blue = (int)(blue*kd*productt + blue*ka+ks*blue*back1*back1);
          int[] maxi=new int[3];
          if(red>255||green>255||blue>255)
          {
              maxi = this.color_range(red,green,blue);
               red= maxi[0];
          blue= maxi[2];
          green=maxi[1];
          }
//          
//         
          if(red<0)red=0;
          if(green<0)green=0;
          if(blue<0)blue=0;
          
          
          
          Color clrs;
          clrs = new Color(red,green,blue);
          g.setColor(clrs);
         // polygon.make_polygon_set_vetices(x1,y1,z1,x2,y2,z2,x3,y3,z3);
          //g.drawPolygon(xx, yy, 3);
          
//          if(back1<0)
          draw_surface(g,xx, yy, 3);
          
          p=temp+1;
//

              
            
        }
     }
     class make_polygon_vertices{
        float x_1,y_1,z_1;
         float x_2,y_2,z_2;
          float x_3,y_3,z_3;
          
           void make_polygon_set_vetices(float x1,float y1,float z1,float x2,float y2,float z2,float x3,float y3,float z3)
          {
              x_1=x1;
              y_1=y1;
              z_1=z1;
              
              x_2=x2;
              y_2=y2;
              z_2=z2;
              
              x_3=x3;
              y_3=y3;
              z_3=z3;
          }
     
    }
     
     
     void paint_planet_sun(float[][]planet_world_coordinate,Graphics g)
     {
         float [][]perspective_coordinate2=returns_perspective_coordinate(z_vp,z_prp,dp,planet_world_coordinate,2592);//2592 points in the sphere with increment of 5
         draw_it(perspective_coordinate2,planet_world_coordinate,z_prp,dp,2592);
         int p=0;
         float x1,y1,z1,x2,y2,z2,x3,y3,z3;
         float a1,b1,c1,a2,b2,c2,a3,b3,c3;
         make_polygon_vertices polygon=new make_polygon_vertices();
         while(p<2519)
         { 
          int temp=p; 
          x1=perspective_coordinate2[0][p];
          a1 = mercury_points[0][p];
          y1=perspective_coordinate2[1][p];
          b1 = mercury_points[1][p];
          z1=perspective_coordinate2[2][p];
          c1 = mercury_points[2][p];
          //System.out.println(p+",");
          p=p+72;
          
          x2=perspective_coordinate2[0][p];
          a2 = mercury_points[0][p];
          y2=perspective_coordinate2[1][p];
          b2 = mercury_points[1][p];
          z2=perspective_coordinate2[2][p];
          c2 = mercury_points[2][p];
           //System.out.println(p+",");
          p=p+1;
          x3=perspective_coordinate2[0][p];
          a3 = mercury_points[0][p];
          y3=perspective_coordinate2[1][p];
          b3 = mercury_points[1][p];
          z3=perspective_coordinate2[2][p];
          c3 = mercury_points[2][p];
          int[]x={(int)x1,(int)x2,(int)x3};
          int[]y={(int)y1,(int)y2,(int)y3};
           //System.out.println(p+"\n");
          Color clr;
          int red,green,blue;
          red = 255;
          green = 255;
          blue = 0;
          
          
          
          
          red = (int)Math.round(red*kd*0.007+red*ka);
          green = (int)Math.round(green*kd*0.007+green*ka);
          blue = (int)Math.round(blue*kd*0.007+blue*ka);
          
          clr = new Color(red,green,blue);
    g.setColor(clr);
               
          g.fillPolygon(x, y, 3);
         
          p=temp;
         x1=perspective_coordinate2[0][p];
          a1 = mercury_points[0][p];
          y1=perspective_coordinate2[1][p];
          b1 = mercury_points[1][p];
          z1=perspective_coordinate2[2][p];
          c1 = mercury_points[2][p];
           //System.out.println(p+",");
          p=p+1;
          
         x2=perspective_coordinate2[0][p];
          a2 = mercury_points[0][p];
          y2=perspective_coordinate2[1][p];
          b2 = mercury_points[1][p];
          z2=perspective_coordinate2[2][p];
          c2 = mercury_points[2][p];
           //System.out.println(p+",");
          p=p+72;
          x3=perspective_coordinate2[0][p];
          a3 = mercury_points[0][p];
          y3=perspective_coordinate2[1][p];
          b3 = mercury_points[1][p];
          z3=perspective_coordinate2[2][p];
          c3 = mercury_points[2][p];
           
          int[]xx={(int)x1,(int)x2,(int)x3};
          int[]yy={(int)y1,(int)y2,(int)y3};
           //System.out.println(p+"\n");
          //Color clrs;
          //int redd,greenn,bluee;
         red = 255;
          green =255;
          blue = 0;
         // _3D_point light_srcc = new _3D_point(1,1,-1);
          
          Color clrs;
          //productt*=4;
          
          red = (int)Math.round(red*kd*0.007 + red*ka);
          green = (int)Math.round(green*kd*0.007 + green*ka);
          blue = (int)Math.round(blue*kd*0.007 + blue*ka);
          

          clrs = new Color(red,green,blue);
    g.setColor(clrs);
          
          g.fillPolygon(xx, yy, 3);
          p=temp+1;

              
            
        }
     }
     
    public void set_background_arrangement(Graphics g){
        this.setBackground(Color.black);
        g.setColor(Color.white);
        g.fillOval(5, 5, 10, 10);
        g.fillArc(10, 14, 25, 30, 30, 50);
    } 
    public void draw_surface(Graphics planet, int[] x, int[] y, int n){
        planet.fillPolygon(x, y, n);
    } 
}
