package AndroidUser;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 import org.json.JSONArray;
import org.json.JSONObject;
import AndroidUser.Udatabase;
public class androidUser extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public androidUser() {
        super();
    }
 
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) 
            throws ServletException, IOException {
        
        String longitude=request.getParameter("longitude");
        String latitude=request.getParameter("latitude");
        //String longitude="85.322668";
        //String latitude="27.665995";
        System.out.println("long and lat : "+longitude+" "+latitude);
        JSONObject ans=new JSONObject();
    
        Udatabase worrior=new Udatabase();
        double longi=Double.parseDouble(longitude);
        double lati=Double.parseDouble(latitude);
        
        PrintWriter out = response.getWriter();
        String policebit=worrior.policebit(longi,lati);
        ans.put("Bit",policebit);
        ans.put("CrimeInfo",worrior.getData(policebit));
        //System.out.println(ans.toString());
        out.println(ans.toString());
    }
}
