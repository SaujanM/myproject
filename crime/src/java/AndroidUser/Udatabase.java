/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AndroidUser;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author SAUJAN
 */
public class Udatabase {
      HashMap< String,JSONObject> hashMap= new HashMap< String,JSONObject>();
    String locality="";
    Connection connection = null;
    boolean status = false;
    Statement stmt = null;
    ArrayList<Integer> num=new ArrayList<Integer>();
    public Udatabase() {
        boolean ss = setuP();
    }

    public boolean setuP() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
            return status;
        }
        System.out.println("MySQL JDBC Driver Registered!");
        //this is important
        try {
            connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/crime", "root", "");
            stmt = connection.createStatement();
            status = true;
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return false;
        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
        return status;
    }
    public void locality(){
         ResultSet rs;
         Statement stmts=null;
         try{
             stmts=connection.createStatement();
         }
         catch(SQLException e){
             System.out.println(e.getMessage());
         }
        int localityid = 0;
        try {
             stmts.executeUpdate("TRUNCATE TABLE androiduser");
             //stmts.executeUpdate("Alter table androiduser auto_increment='"+1+"'");
            rs = stmts.executeQuery("SELECT distinct locationId from crime");
            while (rs.next()) {
                localityid = rs.getInt("locationId");
                sendLocalityId(localityid);
            //System.out.println("Next Locality");
            }

        } catch (SQLException e) {
            System.out.println("ERROR IN Locality " + e.getMessage());
        }
    }
    public void sendLocalityId(int id){
         ResultSet rs;
         Statement stmts;
        stmts = null;
        int timeId=0;//String locality="";
        int crimetypeid=0;
        int crimeid=0;
        
          try{
          stmts=connection.createStatement();
          }
          catch(SQLException e){System.out.println("EditLocation");}
          try {

            rs = stmts.executeQuery("SELECT  policebit from location where locationId='"+id+"'");
           
                
            while (rs.next()) {
                locality = rs.getString("policebit");
              
            }

        } catch (SQLException e) {
            System.out.println("ERROR IN LOcality" + e.getMessage());
        }
     try {

            rs = stmts.executeQuery("SELECT  timeId,crimetypeId,crimeId from crime where locationId='"+id+"'");
            while (rs.next()) {
                timeId = rs.getInt("timeId");
                crimetypeid=rs.getInt("crimetypeId");
                crimeid=rs.getInt("crimeId");
              
                findCrime(id,timeId,crimetypeid);
            }

        } catch (SQLException e) {
            System.out.println("ERROR IN GETTING CRIMEID " + e.getMessage());
        }
    }
    public void findCrime(int Id,int timeId,int crimetypeId){
        ResultSet rs=null;
        Statement stmts=null;
        String year="";
        String timeband="";
   try{
       stmts=connection.createStatement();
   }
   catch(SQLException e){
       System.out.println( e.getMessage());
   }
   String currentYear="2071";
   String pastYear="2070";
 try {

            rs = stmts.executeQuery("SELECT year,timeBand from time where timeId='"+timeId+"' and (year='"+currentYear+"' or year='"+pastYear+"')" );
            while (rs.next()) {
                year=rs.getString("year");
                timeband=rs.getString("timeBand");
                //System.out.println("Locality: "+locality+" Year: "+year+" Timeband: "+timeband);
                if(checkrow(year,crimetypeId)){
                    increment(year,crimetypeId,timeband);
                }
                else {
                        //System.out.println("Making New Row");
                        createrow(year,crimetypeId,timeband);
                }
            }

        } catch (SQLException e) {
            System.out.println("ERROR IN FINDCRIME" + e.getMessage());
        }

}
    public void increment(String year,int crimetypeId,String timeband){
            ResultSet rs=null;
        boolean test=false;
        Statement stmts=null;
        JSONObject glass=new JSONObject();
        try{
            stmts=connection.createStatement();
        }
        catch(SQLException e)
        {
            System.out.println(e.getMessage());
        }
         try {

            rs = stmts.executeQuery("SELECT  details from androiduser where policebit='"+locality+"'  and crimetypeId='"+crimetypeId+"'");
            while (rs.next()) {
                glass=new JSONObject(rs.getString("details"));
               modifyrow( year, crimetypeId,timeband,glass);
            }

        } catch (SQLException e) {
            System.out.println("ERROR IN CHECKROW" + e.getMessage());
        }
        
    }
    public void modifyrow(String year,int crimetypeId,String timeband,JSONObject glass){
             ResultSet rs=null;
        boolean test=false;
        Statement stmts=null;
        try{
            stmts=connection.createStatement();
        }
        catch(SQLException e)
        {
            System.out.println(e.getMessage());
        }
         int count=glass.getInt(timeband);
         count++;
         int total=glass.getInt("Total");
         total++;
         glass.put(timeband, count);
         glass.put("Total", total);
         //System.out.println("After Modification: "+glass);
         try {

             stmts.executeUpdate("UPDATE androiduser SET details='"+glass+"' WHERE policebit='"+locality+"' and crimetypeId='"+crimetypeId+"'");

        } catch (SQLException e) {
            System.out.println("ERROR IN CHECKROW" + e.getMessage());
        }
    }
    public boolean checkrow(String year,int crimetypeid){
        ResultSet rs=null;
        boolean test=false;
        Statement stmts=null;
        try{
            stmts=connection.createStatement();
        }
        catch(SQLException e)
        {
            System.out.println(e.getMessage());
        }
         try {

            rs = stmts.executeQuery("SELECT  policebit from androiduser where policebit='"+locality+"'  and crimetypeId='"+crimetypeid+"'");
            while (rs.next()) {
                test=true;
               
            }

        } catch (SQLException e) {
            System.out.println("ERROR IN CHECKROW" + e.getMessage());
        }
         return test;
    }
    public void createrow(String year,int crimetypeid,String timeband){
        ResultSet rs=null;
        boolean test=false;
        Statement stmts=null;
        try{
            stmts=connection.createStatement();
        }
        catch(SQLException e)
        {
            System.out.println(e.getMessage());
        }
         try {
             JSONObject detail=new JSONObject();
             detail=makeDetails(timeband);
               stmt.executeUpdate("INSERT INTO androiduser(policebit,crimetypeId,details) VALUES (  '" + locality + "','" + crimetypeid + "','" +  detail+ "')");
         //System.out.println(locality+" "+year+" "+detail);    
         }
             catch (SQLException e) {
            System.out.println("ERROR IN CREATEROW" + e.getMessage());
        }
      
    }
    public JSONObject makeDetails(String timeband){
        JSONObject test=new JSONObject();
        int one=1;
        int zero=0;
        test.put("Early Dawn",zero);
        
        test.put("Dawn",zero);
        test.put("Early Morning",zero);
        test.put("Mid Morning",zero);
        test.put("Morning",zero);
        test.put("Early Afternoon",zero);
        
        test.put("Afternoon",zero);
        test.put("Evening",zero);
        test.put("Night",zero);
        test.put("Total",one);
        
   
         Iterator i = test.keys();
         while (i.hasNext()) {
         String key= (String)i.next();
        
         if(key.compareTo(timeband)==0){
           test.put(timeband,one);
         }
         }
         //System.out.println("New Json: "+test);
        
        return test;
    }
    public JSONObject getData(String policebit){
        ResultSet rs;
         Statement stmts=null;
         try{
             stmts=connection.createStatement();
         }
         catch(SQLException e){
             System.out.println(e.getMessage());
         }
         String part=makestring(policebit);
         JSONArray data=new JSONArray();
          JSONArray second=new JSONArray();
         try {

            rs = stmts.executeQuery("SELECT policebit,crimetypeId,details from androiduser where "+part);
            System.out.println("SELECT policebit,crimetypeId,details from androiduser where "+part);
            while (rs.next()) {
                JSONObject Obj=new JSONObject();
               String locality=rs.getString("policebit");
               int id=rs.getInt("crimetypeId");
                 if(!num.contains(id)){num.add(id);}
               String details=rs.getString("details");
               //System.out.println(details);
               Obj.put("policebit",locality);
               Obj.put("crimeid",id);
               Obj.put("detail", details);
               data.put(Obj);
               
            }
    //System.out.println("Data: "+data);
   second=crimetable();
        } catch (SQLException e) {
            System.out.println("ERROR IN GETTING CRIMEID " + e.getMessage());
        }
         JSONObject Finale=new JSONObject();
         Finale.put("androidUser", data);
         Finale.put("crimetable",second);
         System.out.println("Finale: \n"+Finale);
        return Finale;
    }
    public JSONArray crimetable(){
        JSONArray packs=new JSONArray();
        ResultSet rs;
         Statement stmts=null;
         try{
             stmts=connection.createStatement();
         }
         catch(SQLException e){
             System.out.println(e.getMessage());
         }
         int i=0;
         try
         {
            if(num.size()>0)
            {
               for(int j=0;j<num.size();j++)
               {
                rs = stmts.executeQuery("SELECT crimetypeId, crimename, sub_crimename from crimetype where crimetypeId='"+num.get(j)+"'");
                while(rs.next())
                {
                    JSONObject obj=new JSONObject();
                    obj.put("id",rs.getInt("crimetypeId"));
                    obj.put("crimename",rs.getString("crimename"));
                    obj.put("sub_crimename",rs.getString("sub_crimename"));
                    packs.put(obj);
                }
               }
            }
             //System.out.println("Packs: "+packs);
         }
        
         catch(Exception e){
             System.out.println("ERROR IN crimetable "+e.getMessage());
         }
         return packs;
    }
    String makestring(String x){
        String words="(";
       
                words+="policebit ='"+x+"'";
       
        words+=")";
        System.out.println("WOrds to Concatinate "+words);
    return words;
    }
    public String policebit(double x,double y){
        String namePoliceBit="Not Known";
         ResultSet rs;
         Statement stmts=null;
          try{
             stmts=connection.createStatement();
         }
         catch(SQLException e){
             System.out.println(e.getMessage());
         }
         HashMap<String, double[]> memory = new HashMap<String, double[]>();
          try {

            rs = stmts.executeQuery("SELECT lgt,lat,Policebit from police_bit" );
            while (rs.next()) {
               double []cordinates= new double[2];
                double lat=rs.getDouble("lat");
               double lgt=rs.getDouble("lgt");
               cordinates[0]=lgt;
               cordinates[1]=lat;
               String policebit=rs.getString("Policebit");
                memory.put(policebit,cordinates);
               
            }
        namePoliceBit=getPolicebitname(memory,x,y);
   
        }
          catch(SQLException e){
              System.out.println("error in getting policebit\n"+e.getMessage());
          }
        return namePoliceBit;
    }
    private static Double toRad(Double value) {
        return value * Math.PI / 180;
    }
    public String getPolicebitname(HashMap<String,double[]> memory,double lgt,double lat){
        double minDistance=1000000;
        double distance;

        String policebitname="Not Known";
        double R=6371;
        
        for (Entry<String,double[]> entry : memory.entrySet()) {
            double dlong;
            double dlat;
            double a;
            double c;
            double d;
            double []cor=entry.getValue();
            dlong=toRad(lgt-cor[0]);
            dlat=toRad(lat-cor[1]);
            a = Math.sin(dlat/ 2) * Math.sin(dlat / 2) + 
                   Math.cos(toRad(lat)) * Math.cos(toRad(cor[1])) * Math.sin(dlong/2) * Math.sin(dlong/2);
         c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
         d = R * c;
            String name=entry.getKey();
            System.out.println(name+" -fixed(Distance): "+d);
            if(d<minDistance){
                minDistance=d;
                policebitname=name;
            }
        }
     System.out.println("Minimum DIstance of "+policebitname);
        return policebitname;
    }
}