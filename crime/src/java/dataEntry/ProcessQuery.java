/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataEntry;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 *
 * @author SAUJAN
 */
public class ProcessQuery {

    database myDatabase = new database();

    public JSONArray MakeQuery(String crim, String loca, String crimetime) {
        String query ;
        String crime;
        String cause;

        JSONObject e = new JSONObject();
        JSONArray crimetest = new JSONArray(crim);
        for (int i = 0; i < crimetest.length(); i++) {
            e = crimetest.getJSONObject(i);
        }
        if (e.getString("CAUSE").equals("Not Known")) {
            crime = e.getString("CRIME");
            query = "WHERE crimetypeId IN " + "(SELECT crimetypeId FROM crimetype WHERE crimename" + " ='" + crime + "')";
        } else {
            crime = e.getString("CRIME");
            cause = e.getString("CAUSE");
            query = "WHERE crimetypeId IN " + "(SELECT crimetypeId FROM crimetype WHERE crimename" + " ='" + crime + "' and sub_crimename" + " ='" + cause + "')";

        }
                    //CrimeLocation data purification
        //String loca=request.getParameter("Location");
        JSONObject f = new JSONObject();
        JSONArray crimloca = new JSONArray(loca);
        for (int i = 0; i < crimloca.length(); i++) {
            f = crimloca.getJSONObject(i);
        }
        if (f.getString("REGION").equals("")) {
        } else {
            String regio = f.getString("REGION");
            query += "AND locationId IN ";
            if (f.getString("DISTRICT").equals("")) {
                query += "( SELECT locationId FROM location WHERE regionId IN" + "(SELECT regionId FROM region WHERE region " + " ='" + regio + "')";
            } else {
                String District = f.getString("DISTRICT");
                query += "( SELECT locationId FROM location WHERE regionId IN" + "(SELECT regionId FROM region WHERE region " + " ='" + regio + "' and district" + " ='" + District + "')";
            }
            if (!f.getString("POLICEBIT").isEmpty()) {
                query += " AND policebit " + " ='" + f.getString("POLICEBIT") + "'";
            }
            if (!f.getString("POLICEAREA").isEmpty()) {
                query += " AND policearea " + " ='" + f.getString("POLICEAREA") + "'";
            }
            if (!f.getString("LOCALITY").isEmpty()) {
                query += " AND locality " + " ='" + f.getString("LOCALITY") + "'";
            }
            query += ")";
        }
                    //crime time
        //String crimetime=request.getParameter("Crimetime");
        JSONObject g = new JSONObject();
        JSONArray Ctime = new JSONArray(crimetime);
        for (int i = 0; i < Ctime.length(); i++) {
            g = Ctime.getJSONObject(i);
        }
        if (!g.getString("YEAR").isEmpty()) {
            String year = g.getString("YEAR");
            query += "AND timeid in (SELECT timeId FROM time WHERE year" + "='" + year + "'";
            if (g.getString("MONTH").compareTo("Not Known") != 0) {
                query += "AND month=" + "'" + g.getString("MONTH") + "'";
            }
            if (g.getString("DAY").compareTo("Not Known") != 0) {
                query += "AND day=" + "'" + g.getString("DAY") + "'";
            }
            if (g.getString("PERIOD").compareTo("Not Known") != 0) {
                query += "AND timeBand=" + "'" + g.getString("PERIOD") + "'";
            }

            query += ")";
        } else if (g.getString("MONTH").compareTo("Not Known") != 0) {
            String month = g.getString("MONTH");
            query += "AND timeid in (SELECT timeId FROM time WHERE month" + "='" + month + "'";
            if (g.getString("DAY").compareTo("Not Known") != 0) {
                query += "AND day=" + "'" + g.getString("DAY") + "'";
            }
            if (g.getString("PERIOD").compareTo("Not Known") != 0) {
                query += "AND timeBand=" + "'" + g.getString("PERIOD") + "'";
            }
            query += ")";
        } else if (g.getString("DAY").compareTo("Not Known") != 0) {
            String day = g.getString("DAY");
            query += "AND timeid in (SELECT timeId FROM time WHERE day" + "='" + day + "'";

            if (g.getString("PERIOD").compareTo("Not Known") != 0) {
                query += "AND timeBand=" + "'" + g.getString("PERIOD") + "'";
            }
            query += ")";
        } else if (g.getString("PERIOD").compareTo("Not Known") != 0) {
            String period = g.getString("PERIOD");
            query += "AND timeid in (SELECT timeId FROM time WHERE timeBand" + "='" + period + "'";

            query += ")";
        } else {
        }
        System.out.println("Mega Query: "+ query);
        return myDatabase.test(query);
    }

}
