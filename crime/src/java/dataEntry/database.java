package dataEntry;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author SAUJAN
 */
public class database {

    Connection connection = null;
    boolean status = false;
    Statement stmt = null;
   public database(){
       boolean ss=setuP();
   }

   public boolean setuP() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
            return status;
        }
        System.out.println("MySQL JDBC Driver Registered!");
        //this is important
        try {
            connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/crime", "root", "");
            stmt = connection.createStatement();
            status = true;
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return false;
        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
        return status;
    }
   public int getCrimeTypeId(String crime, String subcrime) {
        int id = 0;
        ResultSet rs;
        try {
            rs = stmt.executeQuery("SELECT crimetypeId FROM crimetype WHERE crimename ='" + crime + "' and sub_crimename='" + subcrime + "'");
            while (rs.next()) {
                id = rs.getInt("crimetypeId");
            }

        } catch (SQLException e) {
            System.out.println("ERROR GETTING CRIMETYPEID " + e.getMessage());
        }

        return id;
    }

    public int getlocationid(String region, String district, String policebit, String policearea, String locality) {
        int regionid = getregionid(region, district);
        //System.out.println(regionid);
        ResultSet rs;
        String empty="";
        int locationid = 0;
        //if(region.compareTo(empty)==0)region="Not Known";
        
        //if(region.compareTo(empty)==0)region="Not Known";
        //if(region.compareTo(empty)==0)region="Not Known";
        try {

            rs = stmt.executeQuery("SELECT locationId FROM location WHERE regionId ='" + regionid + "' and policebit='" + policebit + "' and policearea='" + policearea + "' and locality='" + locality + "'");
            while (rs.next()) {
                locationid = rs.getInt("locationId");
                //locationid=2;
            }
            if (locationid == 0) {
                stmt.executeUpdate("INSERT INTO location(regionId,policebit,policearea,locality) VALUES (  '" + regionid + "','" + policebit + "','" + policearea + "','" + locality + "')");
                rs = stmt.executeQuery("SELECT LAST_INSERT_ID() AS locationId");
                while (rs.next()) {
                    locationid = rs.getInt("locationId");
                }
                System.out.print("locationid is " + locationid);
                //locationid=9;
            }
        } catch (SQLException e) {
            System.out.print("ERROR IN GETTING LOCATIONID" + e.getMessage());
            //locationid=8;
        }
        return locationid;
    }

    public int getregionid(String region, String district) {
        int id = 0;
        try {
            ResultSet rs;
            rs = stmt.executeQuery("SELECT regionId FROM region WHERE region ='" + region + "' and district='" + district + "'");
            while (rs.next()) {
                id = rs.getInt("regionId");
            }
            if (id == 0) {
                stmt.executeUpdate("INSERT INTO region(region,district) VALUES (  '" + region + "','" + district + "')");
                rs = stmt.executeQuery("Select LAST_INSERT_ID() as regionId");
                id = rs.getInt("regionId");
            }
        } catch (SQLException e) {
            System.out.print("ERROR IN GETTING REGIONID " + e.getMessage());
        }
        return id;
    }

    public int getTimeId(String year, String month, String day, String hour, String min, String ampm) {
        ResultSet rs;
        String Timeband = GetTimeBand(hour, min, ampm);
        int timeid = 8;
        try {

            stmt.executeUpdate("INSERT INTO time (year,month,day,timeBand,hour,minute,ampm) VALUES (  '" + year + "','" + month + "','" + day + "','" + Timeband + "','" + Integer.parseInt(hour) + "','" + Integer.parseInt(min) + "','" + ampm + "')");
            rs = stmt.executeQuery("select LAST_INSERT_ID() AS timeId");
            while (rs.next()) {
                timeid = rs.getInt("timeId");

            }
            // locationid=9;
            //timeid=9;
            System.out.println(" timeId is" + timeid);
        } catch (SQLException e) {
            System.out.println("ERROR IN GETTING TIMEID and timeId is" + timeid + e.getMessage());
        }
        return timeid;
    }

    public String GetTimeBand(String hour, String min, String ampm) {
        String timeband = "";
        double Hour = Double.parseDouble(hour);
        double Min = Double.parseDouble(min);

        if (ampm.compareTo("AM") == 0) {
            if (Hour <= 5 && Hour > 1) {
                timeband = "Dawn";
            } else if (Hour <= 6 && Hour > 5) {
                timeband = "Early Morning";
            } else if (Hour <=9 && Hour > 6) {
                timeband = "Morning";
            } 
            else if (Hour <= 12 && Hour > 9) {
                timeband = "Mid Morning";
            }else {
                timeband = "Early Dawn";
            }
        } else {
            if (Hour <= 4 && Hour > 1) {
                timeband = "Afternoon";
            } else if (Hour <= 9 && Hour > 4) {
                timeband = "Evening";
            } else if (Hour <= 12 && Hour > 9) {
                timeband = "Night";
            } else {
                timeband = "Early Afternoon";
            }

        }
        return timeband;
    }

    public int getCrimeId(int locationid, int registerid, int timeid, int crimetypeid, double latitude, double longitude) {
        ResultSet rs;
        int crimeid = 0;
        System.out.println("Latitude: "+latitude);
        try {

            stmt.executeUpdate("INSERT INTO crime (crimetypeId,locationId,timeId,registerId,longitude,latitude) VALUES (  '" + crimetypeid + "','" + locationid + "','" + timeid + "','" + registerid + "','" + longitude + "','" + latitude + "')");
            rs = stmt.executeQuery("SELECT LAST_INSERT_ID() AS crimeId");
            while (rs.next()) {
                crimeid = rs.getInt("crimeId");
            }

        } catch (SQLException e) {
            System.out.println("ERROR IN GETTING CRIMEID" + e.getMessage());
        }
        return crimeid;
    }

    public int GetCriminalId(String firstname, String lastname, String gender, String district, int age, String munivdc, String tole) {
        int regionid = Regionid(district);
        Map<String, String> data = new HashMap<String, String>();
        data.put("Municipality", munivdc);
        data.put("Tole", tole);
        JSONObject json = new JSONObject(data);
        //json.putAll( data );
        ResultSet rs;
        int criminalid = 0;
        try {
            stmt.executeUpdate("INSERT INTO criminal(firstName,lastname,age,dob,address,gender,regionId,is_Dummy) VALUES (  '" + firstname + "','" + lastname + "','" + age + "','" + "2048" + "','" + json + "','" + gender + "','" + regionid + "','" + 1 + "')");
            rs = stmt.executeQuery(" SELECT LAST_INSERT_ID() as criminalId ");
            while (rs.next()) {
                criminalid = rs.getInt("criminalId");

            }
        } catch (SQLException e) {
            System.out.println("ERROR IN GETCRIMINALID" + e.getMessage());
        }
        return criminalid;
    }

    public void makeCrimeCriminal(int Criminalid, int crimeid) {
        try {
            stmt.executeUpdate("INSERT INTO crime_criminal(crimeId,criminalId) VALUES (  '" + crimeid + "','" + Criminalid + "')");
        } catch (SQLException e) {
            System.out.println("ERROR IN CrimiCriminalID" + e.getMessage());
        }
    }

    public int GetVictimId(String firstname, String lastname, String gender, String district, int age, String munivdc, String tole) {
        int regionid = Regionid(district);
        Map<String, String> data = new HashMap<String, String>();
        data.put("Municipality", munivdc);
        data.put("Tole", tole);
        JSONObject json = new JSONObject(data);
        ResultSet rs;
        int victimid = 0;
        try {
            stmt.executeUpdate("INSERT INTO victim(firstname,lastname,age,dob,address,gender,regionId,is_Dummy) VALUES (  '" + firstname + "','" + lastname + "','" + age + "','" + "2048" + "','" + json + "','" + gender + "','" + regionid + "','" + 1 + "')");
            rs = stmt.executeQuery(" SELECT LAST_INSERT_ID() as victimId ");
            while (rs.next()) {
                victimid = rs.getInt("victimId");

            }
        } catch (SQLException e) {
            System.out.println("ERROR IN GETVICTIMID" + e.getMessage());
        }
        return victimid;
    }

    public void makeCrimeVictim(int victimid, int crimeid) {
        try {
            stmt.executeUpdate("INSERT INTO crime_victim(crimeId,victimId) VALUES (  '" + crimeid + "','" + victimid + "')");

        } catch (SQLException e) {
            System.out.println("ERROR IN CrimiVictimID" + e.getMessage());
        }
    }

    public int Regionid(String district) {
        int id = 0;
        try {
            ResultSet rs;
            rs = stmt.executeQuery("SELECT regionId FROM region WHERE  district='" + district + "'");
            while (rs.next()) {
                id = rs.getInt("regionId");
            }

        } catch (Exception e) {
            System.out.print("ERROR IN GETTING REGIONID");
        }
        return id;
    }
    public JSONArray getCrimelist() {
        String json = "";
        JSONArray jsArray = new JSONArray();
        List<String> arrayList = new ArrayList<String>();
        int locationId = 0;
        ResultSet rs;
        try {
            rs = stmt.executeQuery("SELECT Name FROM crimetype ");
            while (rs.next()) {
                json = rs.getString("Name");
                arrayList.add(json);
            }
            jsArray = new JSONArray(arrayList);
        } catch (Exception e) {
            System.out.println("getlocationId function ma error");
        }
        return jsArray;
    }
    public Map gethorizontal(String crime,String year){
        Map<String, Integer> data = new HashMap<String, Integer>();
        int total=0;
        String name="";
        try {
            ResultSet rs;
            rs = stmt.executeQuery("SELECT T.total, N.sub_crimename FROM year_total_xy_crime T INNER JOIN crimetype N ON T.crimetypeId =N.crimetypeId where N.crimename='"+crime+"' and T.year='"+year+"'");
            while (rs.next()) {
                 total= rs.getInt("T.total");
                 name=rs.getString("N.sub_crimename");
                 data.put( name, total );
            }
            
        } catch (SQLException e) {
            System.out.print("ERROR IN GETTING REGIONID "+e.getMessage());
        }
        return data;
    }
    public Map getvertical (String crime,String year){
        Map<String, Integer> data = new HashMap <String, Integer>();
        int total=0;
        String name="";
        System.out.println("We are in the getvertical function");
        try {
            ResultSet rs;
             //stmt.executeUpdate("  create  table  temp as SELECT regionId,total FROM year_loc_total where crime ='"+crime+"' and year='"+year+"'");
            rs = stmt.executeQuery("SELECT T.total, R.region FROM year_loc_total T INNER JOIN region R ON R.regionId =T.regionId where T.crime='"+crime+"' and T.year='"+year+"'");
                while(rs.next())
                {
                 total= rs.getInt("T.total");
                 name=rs.getString("R.region");
                 data.put( name, total );
                 System.out.println(total+" "+name);
            }
            
        } catch (SQLException e) {
            System.out.print("ERROR IN GETTING GETVERTICAL "+e.getMessage());
        }
      return data;
        
    }
     public Map getCell(String crime,String subcrime,String year)
    {
        System.out.println("I am in getCell "+crime+" "+subcrime+" "+year);
        int total=0;
        String name="";
        int id=0;
        Map<String, Integer> data = new HashMap <String, Integer>();
        try {
            ResultSet rs;
             //stmt.executeUpdate("  create  table  temp as SELECT regionId,total FROM year_loc_total where crime ='"+crime+"' and year='"+year+"'");
            rs = stmt.executeQuery("SELECT crimetypeId FROM crimetype where crimename='"+crime+"' and sub_crimename='"+subcrime+"'");
                while(rs.next())
                {
                 id= rs.getInt("crimetypeId");
                 System.out.println("crimetypeId is "+id);
            }
                rs = stmt.executeQuery("SELECT R.region,T.total FROM region R INNER JOIN loc_crimetype_total T ON T.locationId=R.regionId where T.crimetypeId='"+id+"' ");
                while(rs.next())
                {
                    
                 total= rs.getInt("T.total");
                 name=rs.getString("R.region");
                 System.out.println(total+""+name);
                 data.put( name, total );
            }
            
        } catch (SQLException e) {
            System.out.print("ERROR IN GETTING GETVERTICAL "+e.getMessage());
        } 
        return data;
    }
       public JSONArray getregionString(String name){
         ResultSet rs;
          JSONArray jsArray = new JSONArray();
         ArrayList<String> names=new ArrayList<String>();
         String test="";
         try{
         rs=stmt.executeQuery("SELECT distinct region FROM region WHERE region LIKE '%" + name + "%'");
         while(rs.next()){
             names.add(rs.getString("region"));
             //System.out.println("hhhhh"+rs.getString("region"));
         }
         jsArray=new JSONArray(names);
                 }
         catch(SQLException e){
         System.out.println("ERROR IN GETSTRING()"+e.getMessage());
         }
             return jsArray;
         
     }
       public List GetSubCrimes(String crimename){
            List<String> Causes = new ArrayList<String>();
           ResultSet rs;
           try{
         rs=stmt.executeQuery("SELECT  sub_crimename FROM crimetype WHERE crimename ='"+crimename+"'");
         while(rs.next()){
             Causes.add(rs.getString("sub_crimename"));
         }
                 }
         catch(SQLException e){
         System.out.println("ERROR IN GETSUBCRIME()"+e.getMessage());
         }
           return Causes;
           
       }
       public JSONArray getDistrictString (String regio){
            ResultSet rs;
          JSONArray jsArray = new JSONArray();
         ArrayList<String> names=new ArrayList<String>();
         String test="";
         try{
         rs=stmt.executeQuery("SELECT distinct district FROM region WHERE region LIKE '%" + regio + "%'");
         while(rs.next()){
             System.out.println(rs.getString("district"));
             names.add(rs.getString("district"));
         }
         jsArray=new JSONArray(names);
                 }
         catch(SQLException e){
         System.out.println("ERROR IN GETSTRING()"+e.getMessage());
         }
             return jsArray;
           
       }
       public JSONObject getlocations(String locality,String area,String sel){
                JSONObject json = new JSONObject();
             
                Map<String, String> data = new HashMap <String, String>();
           ResultSet rs;
             //String queryvar="";
             String display="";
             if(sel.compareTo("area")==0){
              display="T.policearea"+" ='"+area+"'";
             }
             else if(sel.compareTo("locality")==0){
                display="T.locality"+" ='"+locality+"'";
             }
             else display="T.locality"+" ='"+locality+"' and T.policearea"+"='"+area+"'";
           //    return jsArray;
          String query="SELECT distinct R.region,R.district,T.policebit,T.policearea FROM region R INNER JOIN location T ON R.regionId=T.regionId WHERE "+display+"";
          System.out.println("QUERY= "+query);    
          try{
         rs=stmt.executeQuery(query);
         while(rs.next()){
             System.out.println(rs.getString("R.region"));
         data.put("region",rs.getString("R.region"));
         
         data.put("district",rs.getString("R.district"));
         
         data.put("policebit",rs.getString("T.policebit"));
         
         data.put("policearea",rs.getString("T.policearea"));
         }
                 }
         catch(SQLException e){
         System.out.println("ERROR IN GETSTRING()"+e.getMessage());
         }
           json=new JSONObject(data);
               return json;
                       
       }
     public JSONArray test(String query)
       {
           JSONArray array = new JSONArray();
           JSONObject obj=new JSONObject();
       
           int num=0;
           String staru="NOT FOUND";
           System.out.println("we are in test "+query );
         ResultSet rs;   
         try{
         rs=stmt.executeQuery("Select crimeId from crime "+query);
         while(rs.next()){
                 JSONObject bijay= new JSONObject();
             staru="FOUND";num=rs.getInt("crimeId");             
             System.out.println(num);
             //array.put(JSONEDIT(num));
            bijay.put(Integer.toString(num),JSONEDIT(num));
            
            array.put(bijay);
       }
         }
         catch(SQLException e)
         {
             System.out.println(e.getMessage()+"ERROR IN TEST()");
         }
         System.out.println("Select crimeId from crime "+query);
     //return "Select crimeId from crime "+query+"\n"+staru;
     return array;
       }
     //EDIT OPERATION
     public JSONObject EditLocation(int i){
          JSONObject obj = new JSONObject();
          //JSONObject two =new JSONObject();
          Statement bijay=null;
          try{
          bijay=connection.createStatement();
          }
          catch(SQLException e){System.out.println("EditLocation");}
          ResultSet rs;   
         try{
         //rs=stmt.executeQuery("Select * from location where crimeId"+" ='"+i+"'");
         rs=bijay.executeQuery("SELECT R.region,R.district,T.policebit,T.policearea,T.locality FROM region R INNER JOIN location T ON T.regionId=R.regionId where T.locationId in (SELECT locationId from crime where crimeId"+" ='"+i+"')"); 
         while(rs.next()){
            System.out.println(rs.getString("R.region"));
            obj.put("Region",rs.getString("R.region"));
            obj.put("District",rs.getString("R.district"));
            obj.put("Policebit",rs.getString("T.policebit"));
            obj.put("Policearea", rs.getString("T.policearea"));
            obj.put("Locality", rs.getString("T.locality"));
            
       }
         //two.put("Location",obj);
         //System.out.println(two);
         }
         catch(SQLException e)
         {
             System.out.println(e.getMessage()+"ERROR IN EditLocation()");
         }
         return obj;
     }
     public JSONObject EditCrimeTime(int i){
         JSONObject obj = new JSONObject();
          //JSONObject two =new JSONObject();
          Statement bijay=null;
          try{
          bijay=connection.createStatement();
          }
          catch(SQLException e){System.out.println("EditCrimeTime");}
         
          ResultSet rs;   
         try{
         //rs=stmt.executeQuery("Select * from location where crimeId"+" ='"+i+"'");
         rs=bijay.executeQuery("SELECT T.month,T.year,T.day,T.hour,T.minute,T.ampm,T.timeBand FROM time T INNER JOIN crime C ON T.timeId=C.timeId where crimeId"+" ='"+i+"'"); 
         while(rs.next()){
            //System.out.println(rs.getString("R.region"));
            obj.put("Year",rs.getString("T.year"));
            obj.put("Month",rs.getString("T.month"));
            obj.put("Day",rs.getString("T.day"));
            obj.put("Hour", rs.getString("T.hour"));
            obj.put("Minute", rs.getString("T.minute"));
            obj.put("AmPm", rs.getString("T.ampm"));
            obj.put("Timeband", rs.getString("T.timeBand"));
            
       }
         //two.put("Location",obj);
         //System.out.println(two);
         }
         catch(SQLException e)
         {
             System.out.println(e.getMessage()+"ERROR IN EditCrimeTime()");
         }
         return obj;
     }
     public JSONObject EditSingleCriminal(int i){
            JSONObject obj = new JSONObject();
          //JSONObject two =new JSONObject();
             Statement bijay=null;
          try{
          bijay=connection.createStatement();
          }
          catch(SQLException e){System.out.println("EditLocation");}
         
          ResultSet rs;   
         try{
         //rs=stmt.executeQuery("Select * from location where crimeId"+" ='"+i+"'");
         rs=bijay.executeQuery("SELECT * FROM criminal C INNER JOIN region R ON C.regionId=R.regionId where criminalId"+" ='"+i+"'"); 
         while(rs.next()){
            //System.out.println(rs.getString("R.region"));
            obj.put("Firstname",rs.getString("C.firstname"));
            obj.put("Lastname",rs.getString("C.lastname"));
            obj.put("Age",rs.getString("C.age"));
            obj.put("Gender", rs.getString("C.gender"));
            obj.put("Address", rs.getString("C.address"));
            obj.put("Region", rs.getString("R.region"));
            obj.put("District", rs.getString("R.district"));
            
       }
         //two.put("Location",obj);
         //System.out.println(two);
         }
         catch(SQLException e)
         {
             System.out.println(e.getMessage()+"ERROR in EditSingleCriminal()");
         }
         return obj;
         
     }
     public JSONObject EditCriminal(int i){
         JSONObject first= new JSONObject();
         JSONObject second= new JSONObject();
           Statement   bijay = null;
           try{
                  bijay= connection.createStatement();}
           catch(SQLException e){System.out.println(e.getMessage());}
          ResultSet rs;   
         try{
         //rs=stmt.executeQuery("Select * from location where crimeId"+" ='"+i+"'");
         rs=bijay.executeQuery("SELECT criminalId FROM crime_criminal where crimeId"+" ='"+i+"'"); 
         while(rs.next()){
         int j=rs.getInt("criminalId");
           first=EditSingleCriminal(j);
           second.put(Integer.toString(j),first );
         }
         }
         catch(SQLException e){
             System.out.println(e.getMessage()+"ERROR IN EditCriminal function "+second);
         }
         return second;
     }
      public JSONObject EditSingleVictim(int i){
            JSONObject obj = new JSONObject();
          //JSONObject two =new JSONObject();
             Statement bijay=null;
          try{
          bijay=connection.createStatement();
          }
          catch(SQLException e){System.out.println("EditLocation");}
         
          ResultSet rs;   
         try{
         //rs=stmt.executeQuery("Select * from location where crimeId"+" ='"+i+"'");
         rs=bijay.executeQuery("SELECT * FROM victim C INNER JOIN region R ON C.regionId=R.regionId where victimId"+" ='"+i+"'"); 
         while(rs.next()){
            //System.out.println(rs.getString("R.region"));
            obj.put("Firstname",rs.getString("C.firstname"));
            obj.put("Lastname",rs.getString("C.lastname"));
            obj.put("Age",rs.getString("C.age"));
            obj.put("Gender", rs.getString("C.gender"));
            obj.put("Address", rs.getString("C.address"));
            obj.put("Region", rs.getString("R.region"));
            obj.put("District", rs.getString("R.district"));
            
       }
         //two.put("Location",obj);
         //System.out.println(two);
         }
         catch(SQLException e)
         {
             System.out.println(e.getMessage()+"ERROR in EDITsingleVictim()");
         }
         return obj;
         
     }
     public JSONObject EditVictim(int i){
         JSONObject first= new JSONObject();
         JSONObject second= new JSONObject();
                  Statement bijay=null;
          try{
          bijay=connection.createStatement();
          }
          catch(SQLException e){System.out.println("EditLocation");}
         
          ResultSet rs;   
         try{
         //rs=stmt.executeQuery("Select * from location where crimeId"+" ='"+i+"'");
         rs=bijay.executeQuery("SELECT victimId FROM crime_victim where crimeId"+" ='"+i+"'"); 
         while(rs.next()){
         int j=rs.getInt("victimId");
           first=EditSingleVictim(j);
           second.put(Integer.toString(j),first );
         }
         }
         catch(SQLException e){
             System.out.println(e.getMessage() +second);
         }
         return second;
     }
     public JSONObject JSONEDIT(int i){
         //JSONArray tr=new JSONArray();
           JSONObject first= new JSONObject();
           JSONObject second= new JSONObject();
          
//get crime location detail and make json object and push to Json array
                                first=EditLocation(i);
                                second.put("Location",first);
                        
//get crime time detail and make json object and push to json array
                                        first=EditCrimeTime(i);
                                second.put("CrimeTime", first);
                        
         //get criminal info and make json objects and push to json array
                                first=EditCriminal(i);
                                second.put("Criminals",first);
         //get victim info and make json object and push to json array
                                first=EditVictim(i);
                                second.put("Victims",first);
                                System.out.println(second);
                              return second;
     }
}

