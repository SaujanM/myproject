/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataEntry;

import java.sql.Connection;

import java.sql.Statement;
import org.json.JSONArray;
import org.json.JSONObject;
//import com.google.gson.Gson;
//import org.json.JSONArray;

//import org.json.parser.JSONParser;
//import org.json.parser.ParseException;
/**
 *
 * @author SAUJAN
 */
public class dataentry {
    database myDatabase=new database();
    Connection connection = null;
    boolean status = false;
    Statement stmt = null;

    public boolean setuP() {
       return myDatabase.setuP();
    }



    public String crime(String crimetype, String location, String time, String criminallist,String victimlist) {
        String empty="";
       // GET CRIMETYPEID
        String Crime="";
        String Subcrime="";
         JSONArray crimetest = new JSONArray(crimetype);
         for(int i=0;i<crimetest.length();i++){
          JSONObject e = crimetest.getJSONObject(i);
          Crime=e.getString("Crime");
          Subcrime=e.getString("Cause");
          }
         //gives crimetypeid
         int crimetypeid=myDatabase.getCrimeTypeId(Crime,Subcrime);
////       
////         GET LOCATIONID
         JSONArray locationtest = new JSONArray(location);
         String region="";String district="";String policebit="";String policearea="";String locality="";
         double longitude=0;double latitude=0;
         for(int j=0;j<locationtest.length();j++){
          JSONObject e = locationtest.getJSONObject(j);
         
          region=e.getString("Region");if(region.compareTo("")==0)region="Not Known";
          district=e.getString("District");
          policebit=e.getString("PoliceBit");
          policearea=e.getString("Policearea");
          locality=e.getString("Locality");
          longitude=e.getDouble("Longitude");
          //System.out.println(tess);
          latitude=e.getDouble("Latitude");
        }
        int locationid=myDatabase.getlocationid(region,district,policebit,policearea,locality);
//       //GET TIMEID
          JSONArray timetest = new JSONArray(time);
         String year=""; String month=""; String day=""; String hour="";String min=""; String ampm="";
       
         for(int j=0;j<timetest.length();j++){
          JSONObject e = timetest.getJSONObject(j);
         
          year=e.getString("Year");
          month=e.getString("Month");
          day=e.getString("Day");
          hour=e.getString("Hour");
          min=e.getString("Min");
          ampm=e.getString("AMPM");
        }
         int timeid=myDatabase.getTimeId( year, month, day, hour, min, ampm);
         int crimeid=myDatabase.getCrimeId(locationid,1,timeid,crimetypeid,latitude,longitude);
         //return victimlist+criminallist;
         if(!criminallist.equals("[]"))
                  Criminal(criminallist, crimeid);
         else System.out.println("criminal empty ");
         if(!victimlist.equals("[]"))
             Victims(victimlist,crimeid);
         else System.out.println("victimlist empty");
         return "ready";
    }



    //criminal detail
    public void Criminal(String criminalist, int crimeid) {
        JSONArray criminals = new JSONArray(criminalist);
        String firstname = "";
        String lastname = "";
        String gender = "";
        String district = "";
        String age = "";
        String munivdc = "";
        String tole = "";
        int criminalid = 0;
        for (int j = 0; j < criminals.length(); j++) {
            JSONObject e = criminals.getJSONObject(j);

            firstname = e.getString("FirstName");
            lastname = e.getString("LastName");
            gender = e.getString("Gender");
            district = e.getString("District");
            age = e.getString("Age");
            munivdc = e.getString("MuniVDC");
            tole = e.getString("Tole");
            criminalid = myDatabase.GetCriminalId(firstname, lastname, gender, district, Integer.parseInt(age), munivdc, tole);
            myDatabase.makeCrimeCriminal(criminalid, crimeid);
        }
       
    }



    public void Victims(String victimlist, int crimeid) {
        JSONArray victims = new JSONArray(victimlist);
        String firstname = "";
        String lastname = "";
        String gender = "";
        String district = "";
        String age = "";
        String munivdc = "";
        String tole = "";
        int victimid = 0;
        for (int j = 0; j < victims.length(); j++) {
            JSONObject e = victims.getJSONObject(j);

            firstname = e.getString("FirstName");
            lastname = e.getString("LastName");
            gender = e.getString("Gender");
            district = e.getString("District");
            age = e.getString("Age");
            munivdc = e.getString("MuniVDC");
            tole = e.getString("Tole");
            victimid = myDatabase.GetVictimId(firstname, lastname, gender, district, Integer.parseInt(age), munivdc, tole);
            myDatabase.makeCrimeVictim(victimid, crimeid);

        }
        //return victimlist+"victimid="+victimid;
    }

    
}
