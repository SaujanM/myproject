<%-- 
    Document   e_entry
    Created on : May 21, 2014, 10:15:59 PM
    Author     : B
--%>

<!DOCTYPE html>
<html>
    <head>
        <title>Crime Control System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!--Google Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Duru+Sans|Actor' rel='stylesheet' type='text/css'>

        <!--Bootshape-->
        <link href="css/bootshape.css" rel="stylesheet">

        <%@include file="header.jsp" %>
    </head>
    <body>

        <!-- Navigation bar -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><span class="green">Crime control</span> System</a>
                </div>
                <nav role="navigation" class="collapse navbar-collapse navbar-right">

                    <ul class="navbar-nav nav">
                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input name="search" type="text" class="form-control" placeholder="Search Database">
                            </div> <button type="submit" class="btn btn-default" href="#search">Go</button>

                        </form>

                        <div class="navbar-nav nav">
                            <% if ((Boolean) session.getAttribute("loggedIn")) {%>
                            <h4>Hi,<b><%=session.getAttribute("userid")%></b> <a href='logout.jsp'>Log out</a></h4>
                            <% } else { %>
                            <a href='login.jsp'>Login</a>
                            <% } %>

                        </div>


                    </ul>
                </nav>
            </div>
        </div><!-- End Navigation bar -->




        <div class="container-fluid">
            <div class="row">
                <div id="mtabs"class="col-sm-3 col-md-2 sidebar" >
                    <ul class="nav nav-sidebar">
                        <li><a href="#dataentry" rel="dataentry">Data Entry</a></li>
                        <li><a href="#visual" rel="visual">Visualization</a></li>
                        <li><a href="#edit" rel="edit">Edit</a></li>
                        <li class="active"><a href="#tab4" rel="tab4">Update For Android User</a></li>
                    </ul>
                </div>
                <div id="mtabs_content_container" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <div id="dataentry" class="mtab_content">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="crime" class="col-sm-2 control-label">Crime</label>
                                <div class="col-md-3">
                                    <%@ page import="dataEntry.dataentry" %>
                                    <%
                                        dataentry obj = new dataentry();
                                        if (!obj.setuP()) {
                                            out.print("Fail in Database Connection");
                                        }
                                    %>
                                    <select onchange="feedOther(this.value, 'cause')" id="crime" class="form-control input-sm">
                                        <option value="Murder">Murder</option>
                                        <option value="Robbery">Robbery</option>
                                        <option value="Suicide">Suicide</option>
                                        <option value="Economic">Economic</option>
                                        <option value="Social Crimes">Social Crimes</option>
                                        <option value="Various">Various</option>
                                        <option value="Women and Children">Child,Woman Violence</option>

                                        <option value="Social Crimes">Social Crimes</option>
                                    </select>
                                </div>

                                <label for="subcrime" class="col-sm-2 control-label">Cause:</label>
                                <div  class="col-md-3">
                                    <select id="cause" class="form-control input-sm">
                                        <option value="">Cause</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="location" class="col-sm-2 control-label">Location:</label>
                                <div class="col-sm-3">
                                    <input onkeyup="regions('region')" type="text" class="form-control" id="region" placeholder="Region">
                                </div>
                                <label for="district" class="col-sm-2 control-label">District:</label>
                                <div class="col-sm-3">
                                    <input onkeyup="calldistrict('')"type="text"  class="form-control" id="district" placeholder="district">
                                </div>

                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Police Bit:</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="policebit" placeholder="Police Bit">
                                </div>
                                <label  class="col-sm-2 control-label">Police Area:</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="policearea" placeholder="Policce Area">
                                </div>


                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Locality:</label>

                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="locality" placeholder="Locality">
                                </div>
                                <label  class="col-sm-2 control-label">Longitude & Latitude:</label>
                                <div class="col-sm-2">
                                    <input type="number" class="form-control" id="lgt" placeholder="Longitude" value="0.0">
                                </div>                       
                                <div class="col-sm-2">
                                    <input type="number" class="form-control" id="ltt" placeholder="Latitude" value="0.0">
                                </div>

                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label"></label>
                                <div class="col-sm-2">
                                    <button onclick="autogenerate('')"type="button" class="btn btn-success">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Auto Generate</button>
                                </div>
                            </div>
                            <hr style="height:1px;border:none;color:#333;background-color:#333;"/>
                            <span align="left"><h3>Criminal Info</h3></span>
                            <div id="CriminalInfo" class="container">

                                <ul id="tab" class="nav nav-tabs">
                                    <li class="active"><a href="#CriminalInfos" data-toggle="tab">Criminal Info</a></li>
                                    <li><a href="#Entries" data-toggle="tab">Entries</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="CriminalInfos">
                                        <br><br>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">First Name:</label>
                                            <div class="col-md-3">

                                                <input id="Cfirstname" type="text" class="form-control" placeholder="First Name">
                                            </div>

                                            <label  class="col-sm-2 control-label">Last Name:</label>
                                            <div class="col-md-3">
                                                <input id="Clastname" type="text" class="form-control" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-sm-2 control-label">Gender:</label>
                                            <div class="col-md-3">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="criminalgender" id="optionmale" value="Male" checked>
                                                        Male
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="criminalgender" id="optionFemale" value="Female">
                                                        Female
                                                    </label>
                                                </div>

                                            </div>

                                            <label  class="col-sm-2 control-label">Age:</label>
                                            <div class="col-md-3">
                                                <input id="Cage" type="number" class="form-control" placeholder="Age">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">VDC/Municipality</label>
                                            <div class="col-md-3">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="CriminalMuniVDC" id="Muni" value="Municipality" checked>
                                                        Municipality
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="CriminalMuniVDC" id="VDC" value="VDC">
                                                        VDC
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="CriminalMuniVDC" id="notnone" value="Not None">
                                                        Not None
                                                    </label>
                                                </div>

                                            </div>

                                            <label  class="col-sm-2 control-label">District:</label>
                                            <div class="col-md-3">
                                                <input id="Cdistrict" type="text" class="form-control" placeholder="District">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label  class="col-sm-2 control-label">Tole:</label>

                                            <div class="col-md-3">
                                                <input id="Ctole" type="text" class="form-control" placeholder="Tole">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label  class="col-sm-2 control-label"></label>

                                            <div class="col-md-3">
                                                <button  type="button" class="btn btn-danger" onclick="addToCriminalList()">Add</button>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="tab-pane fade" id="Entries">

                                        <table class="table" id="criminaltable">
                                            <th>S.N</th><th>First Name</th><th>Last Name</th><th>Gender</th><th>District</th><th>Date Of Birth</th><th>VDC/muni</th><th>Tole</th>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tab3">
                                        <p>Mauris id mi nisi, et pulvinar erat. Donec tortor nunc, ultricies eu adipiscing eget, varius non quam. Integer ullamcorper arcu mi, eget porta arcu. Pellentesque sodales massa non augue vehicula a molestie tellus tincidunt. Vestibulum et sem velit. Duis in ligula porta magna gravida feugiat ut eu diam. Maecenas ut est id dolor ullamcorper aliquam ut sed est. Aliquam erat volutpat. Vestibulum mauris sapien, ultricies nec convallis tempor, pharetra fringilla orci. Aenean lacus odio, facilisis eu auctor sit amet, luctus vitae risus.</p>
                                    </div>
                                </div>



                            </div>
                            <hr style="height:1px;border:none;color:#333;background-color:#333;"/>
                            <span align="left"><h3>Victim Info</h3></span>
                            <div id="VictimInfo" class="container">

                                <ul id="tab" class="nav nav-tabs">
                                    <li class="active"><a href="#VictimInfos" data-toggle="tab">Victim Info</a></li>
                                    <li><a href="#VEntries" data-toggle="tab">Entries</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="VictimInfos">
                                        <br><br>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">First Name:</label>
                                            <div class="col-md-3">

                                                <input id="Vfirstname" type="text" class="form-control" placeholder="First Name">
                                            </div>

                                            <label  class="col-sm-2 control-label">Last Name:</label>
                                            <div class="col-md-3">
                                                <input id="Vlastname" type="text" class="form-control" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-sm-2 control-label">Gender:</label>
                                            <div class="col-md-3">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="victimgender" id="Voptionmale" value="Male" checked>
                                                        Male
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="victimgender" id="VoptionFemale" value="Female">
                                                        Female
                                                    </label>
                                                </div>

                                            </div>

                                            <label  class="col-sm-2 control-label">Age:</label>
                                            <div class="col-md-3">
                                                <input id="Vage" type="number" class="form-control" placeholder="Age">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">VDC/Municipality</label>
                                            <div class="col-md-3">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="VictimMuniVDC" id="VMuni" value="Municipality" checked>
                                                        Municipality
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="VictimMuniVDC" id="VVDC" value="VDC">
                                                       VDC
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="VictimMuniVDC" id="Vnotnone" value="Not None">
                                                        Not None
                                                    </label>
                                                </div>

                                            </div>

                                            <label  class="col-sm-2 control-label">District:</label>
                                            <div class="col-md-3">
                                                <input id="Vdistrict" type="text" class="form-control" placeholder="District">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label  class="col-sm-2 control-label">Tole:</label>

                                            <div class="col-md-3">
                                                <input id="Vtole" type="text" class="form-control" placeholder="Tole">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label  class="col-sm-2 control-label"></label>

                                            <div class="col-md-3">
                                                <button  type="button" class="btn btn-danger" onclick="addToVictimList()">Add</button>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="tab-pane fade" id="VEntries">

                                        <table class="table" id="victimtable">
                                            <th>S.N</th><th>First Name</th><th>Last Name</th><th>Gender</th><th>District</th><th>Date Of Birth</th><th>VDC/muni</th><th>Tole</th>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tab3">
                                        <p>Mauris id mi nisi, et pulvinar erat. Donec tortor nunc, ultricies eu adipiscing eget, varius non quam. Integer ullamcorper arcu mi, eget porta arcu. Pellentesque sodales massa non augue vehicula a molestie tellus tincidunt. Vestibulum et sem velit. Duis in ligula porta magna gravida feugiat ut eu diam. Maecenas ut est id dolor ullamcorper aliquam ut sed est. Aliquam erat volutpat. Vestibulum mauris sapien, ultricies nec convallis tempor, pharetra fringilla orci. Aenean lacus odio, facilisis eu auctor sit amet, luctus vitae risus.</p>
                                    </div>
                                </div>



                            </div>
                            <hr style="height:1px;border:none;color:#333;background-color:#333;"/>
                            <span align="left"><h3>Time</h3></span>
                            <div id="time">
                                <div class="form-group">
                                    <label for="Victimfirstname" class="col-sm-1 control-label">Year:</label>
                                    <div class="col-md-2">

                                        <input type="text" id="year" class="form-control" placeholder="Year(B.S)">
                                    </div>

                                    <label for="VictimLastname" class="col-sm-1 control-label">Month:</label>
                                    <div class="col-md-2">
                                        <select  id="month" class="form-control input-sm">
                                       <option value="Baisakh">Baisakh</option>
                                        <option value="Jestha">Jestha</option>
                                        <option value="Ashadh">Ashadh</option>
                                        <option value="Shrawan">Shrawan</option>
                                        <option value="Bhadra">Bhadra</option>
                                        <option value="Asoj">Asoj</option>
                                        <option value="Kartik">Kartik</option>
                                        <option value="Mangsir">Mangsir</option>
                                        <option value="Poush">Poush</option>
                                        <option value="Magh">Magh</option>
                                        <option value="Falgun">Falgun</option>
                                        <option value="Chaitra">Chaitra</option>
                                        <option value="Not Known">Not Known</option>
                                    </select>
                                    </div>
                                    <label for="VictimLastname" class="col-sm-1 control-label">Day:</label>
                                    <div class="col-md-2">
                                               <select  id="day" class="form-control input-sm">
                                        <option value="Sunday">Sunday</option>
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thursday">Thursday</option>
                                        <option value="Friday">Friday</option>
                                        <option value="Saturday">Saturday</option>

                                        <option value="Not Known">Not Known</option>
                                    </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="tole" class="col-sm-2 control-label">Approximate Time:</label>

                                    <div class="col-md-2">
                                        <input type="text" id="hour" class="form-control" placeholder="Hour">

                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" id="minute"class="form-control" placeholder="Min">

                                    </div>
                                    <div class="col-md-2">
                                        <select id="ampm" class="form-control input-sm">
                                            <option value="">AM</option>
                                            <option value="volvo">PM</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <button  type="button" class="btn btn-danger" onclick="submitall()">Submit</button>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div id="visual" class="mtab_content">
                        <br>
                        Crime:&nbsp&nbsp&nbsp<select id="horizontalcrime" onchange="feedOther(this.value, 'disabledSelect')" >
                            <option value="Murder">Murder</option>
                            <option value="Robbery">Robbery</option>
                            <option value="Suicide">Suicide</option>
                            <option value="Economic">Economic</option>
                            <option value="Social Crimes">Social Crimes</option>

                            <option value="Various">Various</option>
                        </select>

                        Year:&nbsp&nbsp&nbsp<select id="horizontalyear" >
                            <option value="">065/066</option>
                        </select>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button onclick="horizontal()"type="button" class="btn btn-success">Visualize</button>
                        <br>
                        <div id="checkit">
                            <fieldset disabled class="col-lg-3" id="disableAble">  
                                Cause:&nbsp&nbsp&nbsp<select id="disabledSelect">
                                    <option>Cause</option>
                                </select>

                            </fieldset>
                        </div>
                        <div class="col-lg-2">
                            <input type="radio"  onclick="makeValid('able')"name="Select" value="CauseRegion">&nbsp&nbsp Enable Cause
                            <br><input type="radio" onclick="makeValid('disable')"name="Select" value="Region">&nbsp&nbsp Regions<br>
                            <input type="radio" onclick="makeValid('disable')"name="Select" value="Cause">&nbsp&nbsp Causes

                        </div>
                        <div class="col-lg-2">


                        </div>
                        <br><br>



                        <!--div id="load" class="progress">
   <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
    0%
  </div>
</div-->
                        <br><br>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="container" ></div>
                            </div>
                            <div class="col-md-6">
                                <div id="containerpie"></div>
                            </div>
                        </div>

                    </div>
                    <div id="edit" class="mtab_content">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <br>
                                <label for="crime" class="col-sm-2 control-label">Crime:</label>
                                <div class="col-md-3">
                                    <select onchange="feedOther(this.value, 'Editcause')" id="Editcrime" class="form-control input-sm">
                                        <option value="Murder">Murder</option>
                                        <option value="Robbery">Robbery</option>
                                        <option value="Suicide">Suicide</option>
                                        <option value="Economic">Economic</option>
                                        <option value="Social Crimes">Social Crimes</option>
                                        <option value="Various">Various</option>
                                        <option value="Women and Children">Child,Woman Violence</option>

                                        <option value="Social Crimes">Social Crimes</option>
                                    </select>
                                </div>
                                <label for="subcrime" class="col-sm-2 control-label">Cause:</label>
                                <div  class="col-md-3">
                                    <select id="Editcause" class="form-control input-sm">
                                        <option value="">Cause</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="location" class="col-sm-2 control-label">LocationTest:</label>
                                <div class="col-sm-3">
                                    <input  type="text" class="form-control" id="Eregion" placeholder="Region">
                                </div>
                                <label for="district" class="col-sm-2 control-label">District:</label>
                                <div class="col-sm-3">
                                    <input type="text" onkeyup="calldistrict('E')" class="form-control" id="Edistrict" placeholder="district">
                                </div>

                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Police Bit:</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="Epolicebit" placeholder="Police Bit">
                                </div>
                                <label  class="col-sm-2 control-label">Police Area:</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="Epolicearea" placeholder="Policce Area">
                                </div>


                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Locality:</label>

                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="Elocality" placeholder="Locality">
                                </div>
                                <div class="col-sm-3">
                                    <button onclick="autogenerate('E')"type="button" class="btn btn-success">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Auto Generate</button>
                                </div>
                            </div>
                            <hr style="height:1px;border:none;color:#333;background-color:#333;"/>
                            <h3>Time:</h3>
                            <div class="form-group">
                                <label for="Victimfirstname" class="col-sm-1 control-label">Year:</label>
                                <div class="col-md-2">

                                    <input type="text" id="Eyear" class="form-control" placeholder="Year(B.S)">
                                </div>

                                <label for="VictimLastname" class="col-sm-1 control-label">Month:</label>
                                <div class="col-md-2">
                                    <!--input type="text" id="Emonth" class="form-control" placeholder="Month"-->
                                    <select id="Emonth" class="form-control input-sm">
                                        <option value="Baisakh">Baisakh</option>
                                        <option value="Jestha">Jestha</option>
                                        <option value="Ashadh">Ashadh</option>
                                        <option value="Shrawan">Shrawan</option>
                                        <option value="Bhadra">Bhadra</option>
                                        <option value="Asoj">Asoj</option>
                                        <option value="Kartik">Kartik</option>
                                        <option value="Mangsir">Mangsir</option>
                                        <option value="Poush">Poush</option>
                                        <option value="Magh">Magh</option>
                                        <option value="Falgun">Falgun</option>
                                        <option value="Chaitra">Chaitra</option>
                                        <option value="Not Known">Not Known</option>

                                    </select>
                                </div>
                                <label for="VictimLastname" class="col-sm-1 control-label">Day:</label>
                                <div class="col-md-2">
                                    <!--input type="text" id="Eday" class="form-control" placeholder="Day"-->
                                    <select class="form-control input-sm" id="Eday">
                                        <option value="Sunday">Sunday</option>
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thursday">Thursday</option>
                                        <option value="Friday">Friday</option>
                                        <option value="Saturday">Saturday</option>
                                        <option value="Not Known">Not Known</option>

                                    </select>

                                </div>
                                <div class="col-md-3">
                                    <!--input type="text" id="Eday" class="form-control" placeholder="Day"-->
                                    <select class="form-control input-sm" id="Etimeband">
                                        <option value="Not Known">Not Known</option>
                                        <option value="Sunday">Dawn(1am-5am)</option>
                                        <option value="Monday">Early Morning(5am-6am)</option>
                                        <option value="Tuesday">Morning(6am to 12am)</option>
                                        <option value="Wednesday">Afternoon(1pm-4pm)</option>
                                        <option value="Thursday">Evening(4pm-9pm)</option>
                                        <option value="Friday">Night(9pm-12pm)</option>

                                    </select>

                                </div>
                            </div>
                            <input type="button" onclick="getid()" value="Search"/>
                            <div id="table">
                                <div class="table-responsive">
                                    <h2>Search Results...</h2>
                                    <table class="table" id="edittable">
                                        <th>S.N</th><th>Criminal Details</th><th>Victim Details</th><th>Time</th><th>Location</th>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="tab4" class="mtab_content" style="display: block;">
                        <p>Update Database For Android User</p>
                            <button onclick="update()"type="button" class="btn btn-success">&nbsp&nbsp&nbsp Update</button>
                                
                    </div>

                </div>

            </div>


        </div>

    </body>>
</html>
