<!DOCTYPE html>
<html>
    <head>
        <title>Crime Control System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap --><link href="css/bootstrap.css" rel="stylesheet">

        <!--Google Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Duru+Sans|Actor' rel='stylesheet' type='text/css'>

        <!--Bootshape-->
        <link href="css/bootshape.css" rel="stylesheet">

        <%@include file="header.jsp" %>

    </head>
    <body>
        <!-- Navigation bar -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><span class="green">Crime control</span> System</a>
                </div>
                <nav role="navigation" class="collapse navbar-collapse navbar-right">

                    <ul class="navbar-nav nav">
                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input name="search" type="text" class="form-control" placeholder="Search Database">
                            </div> <button type="submit" class="btn btn-default" href="#search">Go</button>
                        </form>
                                
                        <div class="navbar-nav nav">
                            
                            <% String  userid=(String)session.getAttribute("userid");
                            if(userid==null){
                                %>
                                  <form  class="navbar-form navbar-left" id="form1" method="post" action="login.jsp">
                            <div class="form-horizontal">
                            <input class="input-small" name="username" type="text" placeholder="User name" id="headerlogin">
                            <input class="input-small" name="password" type="password" placeholder="Password" id="headerlogin">
                            <button class="btn btn-default" type="submit" id="">Login</button>
                            <a class="btn btn-default" type="button" href="register.jsp" id="" >Register</a>
                            </div>
                        </form>    
                            <% }
                            
                            else {  %> 
                             <div class="navbar-right">                       
                              <h4> Hi,<b><%=session.getAttribute("userid")%></b> <a href='logout.jsp'>Log out</a></h4> 
                              <% response.sendRedirect("crime_entry.jsp");%>
                              </div>
                                                   
                          <%} %>
                         </div>
                         
                    </ul>
                </nav>
            </div>
        </div><!-- End Navigation bar -->

        <!-- Slide gallery -->
        <br>
        <br>
     <div id="CriminalInfo" class="container">

                                <ul id="tab" class="nav nav-tabs">
                                    <li class="active"><a href="#CriminalInfos" data-toggle="tab">About Us</a></li>
                                    <li><a href="#Entries" data-toggle="tab">Team Members</a></li>
                                    
                                    <li><a href="#Objective" data-toggle="tab">Objective</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="CriminalInfos">
                                        <br><br>
                                       This is Crime Control System designed to strengthen the security system of Nepal.

                                    </div>

                                    <div class="tab-pane fade" id="Entries">

                                            Saujan Maka<br>
                                            
                                            Bijay Kushwaha<br>
                                            
                                            Bikash Bhandari<br>
                                    </div>
                                    <div class="tab-pane fade" id="tab3">
                                    kadjfl;kj
                                    </div>
                                </div>



                            </div>
        <h3 class="text-center">Welcome to Crime Control System</h3>

        <!-- Content -->
        <div class="container">
            <h3 class="text-center">Visualize the Crime Data and analyze the risky place through the  <a class="green" href="#">App</a></h3>
            <p></p>
        </div><!-- End Content -->
        <!-- Footer -->
        <div class="footer text-center">
            <p>Crime Control Group</p>
        </div><!-- End Footer -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- // <script src="https://code.jquery.com/jquery.js"></script> -->
        <script src="js/jquery.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootshape.js"></script>
    </body>
</html>