<!DOCTYPE html>
<html lang="en">
<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Duru+Sans|Actor' rel='stylesheet' type='text/css'>
    
    <!--Bootshape-->
    <link href="css/bootshape.css" rel="stylesheet">

<link href="css/style.css" rel="stylesheet">
<link href="css/responsive-video.css" rel="stylesheet">  
<link href="css/tab.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div class="container">
  <div class="row-fluid">
    <div class="span12">
    
    </div>
  </div>
  <div class="row-fluid">
  <div class="span6 offset6">
    <div id="maincontent" class="span8"> 
      
        <form id="registration-form" class="form-horizontal" method="post" action="register_success.jsp">
       
          <h1><small>Fill up the forms to get register</small></h1>
          <br/>
          
          <div class="form-control-group">
            <label class="control-label" for="name">First Name</label>
            <div class="controls">
              <input type="text" class="input-xlarge" name="fname" id="name">
            </div>
          </div>
          
          <div class="form-control-group">
            <label class="control-label" for="name">Last Name</label>
            <div class="controls">
              <input type="text" class="input-xlarge" name="lname" id="name">
            </div>
          </div>
          
          <div class="form-control-group">
            <label class="control-label" for="name">User Name</label>
            <div class="controls">
              <input type="text" class="input-xlarge" name="username" id="username">
            </div>
          </div>
          
          
          <div class="form-control-group">
            <label class="control-label" for="name">Password</label>
            <div class="controls">
              <input type="password" class="input-xlarge" name="password" id="password">
            </div>
          </div>
          
          <div class="form-control-group">
            <label class="control-label" for="name"> Retype Password</label>
            <div class="controls">
              <input type="password" class="input-xlarge" name="confirm_password" id="confirm_password">
            </div>
          </div>
          
          <div class="form-control-group">
            <label class="control-label" for="email">Email Address</label>
            <div class="controls">
              <input type="text" class="input-xlarge" name="email" id="email">
            </div>
          </div>
      
      <div class="form-control-group">
            <label class="control-label" for="message">Gender</label>
            <div class="controls">
             <input type="text" class="input-xlarge" name="gender" id="gender">
            </div>
          </div>
      
          <div class="form-control-group">
            <label class="control-label" for="message">Your Address</label>
            <div class="controls">
             <input type="text" class="input-xlarge" name="address" id="department">
            </div>
          </div>
          
      <div class="form-control-group">
            <label class="control-label" for="message">Department</label>
            <div class="controls">
              <input type="text" class="input-xlarge" name="department" id="department">
            </div>
          </div>
      
      <div class="form-control-group">
            <label class="control-label" for="message">Identity</label>
            <div class="controls">
             <input type="text" class="input-xlarge" name="identity" id="identity">
            </div>
          </div>
          
           <div class="form-control-group">
            <label class="control-label" for="message">Contact No:</label>
            <div class="controls">
             <input type="text" class="input-xlarge" name="contact" id="contact">
            </div>
          </div>
      
          <div class="form-control-group">
            <label class="control-label" for="message"> Please agree to our policy</label>
            <div class="controls">
             <input id="agree" class="checkbox" type="checkbox" name="agree">
            </div>
          </div>
          
          <div class="form-actions">
            <button type="submit" class="btn btn-success btn-large">Register</button>
            <button type="reset" class="btn">Cancel</button>
          </div>
  
      </form>
    </div>
    <!-- .span --> 
  </div>
  <!-- .row -->
  
</div>
<!-- .container --> 

<script src="js/jquery-1.7.1.min.js"></script> 

<script src="js/jquery.validate.js"></script> 

<script src="js/script.js"></script> 
<script>
    addEventListener('load', prettyPrint, false);
    $(document).ready(function(){
    $('pre').addClass('prettyprint linenums');
      });
    </script> 

</body>
</html>
